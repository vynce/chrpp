/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <iostream>
#include <string>
#include <chrpp.hh>

#include "options.hpp"

/**
 * @brief LEQ example
 *
 * Simple CHR example to implement properties of the leq operator.
 * \ingroup Examples
 *
	<CHR name="LEQ" template_parameters="typename E">
		<chr_constraint> leq(?E,?E)
		reflexivity  @ leq(X, X) <=> success();;
		antisymmetry @ leq(X, Y),  leq(Y, X) <=> X %= Y;;
		idempotence  @ leq(X, Y) \ leq(X, Y) <=> success();;
		transitivity @ leq(X, Y),  leq(Y, Z) ==> leq(X, Z);;
	</CHR>
 */

template < typename T >
void print(T& pb)
{
	auto it = pb.chr_store_begin();
	while (!it.at_end())
	{
		std::cout << it.to_string() << std::endl;
		++it;
	}
}

int main(int, const char **)
{
    TRACE( chr::Log::register_flags(chr::Log::ALL); )

	chr::Logical_var< int > X, Y, Z;
	auto space = LEQ< int >::create();
	CHR_RUN(
        space->leq(X,Y);
        space->leq(Z,X);
        space->leq(Y,Z);
	)
	print(*space);
	chr::Statistics::print(std::cout);
	return 0;
}
