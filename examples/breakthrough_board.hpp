/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <array>
#include <vector>
#include <unordered_set>
#include <fstream>
#include <sstream>
#include <cassert>

/** 
 * Board definition. Each square of the board is numbered and stores a piece data.
 * We use a T_Board element composed of two uint64_t. On first uint, we store the status of
 * the square : 0 if square is free, 1 if square is occupied. On second uint_64, we store the color
 * of the piece : 1 for a white piece and 2 for a black one. 
 * As a result, we can tackle boards up to 64 squares on a T_Board element.
 */
using T_UINT = std::uint64_t;
using T_Board = std::array< T_UINT, 2 >; // We limit to boards up to 64 squares

const unsigned int WHITE = 1; ///< White player
const unsigned int BLACK = 2; ///< Black player
const uint64_t MASK = 1u;	  ///< Mask for extracting a square number from a T_Board element

/** 
 * @brief get_value returns the value of the wanted square \a n of \a board
 * @param board is the board of the game
 * @param p is the number of the dark square to check
 * @return the value of the wanted dark square:
 * - 0 : the square is free
 * - 1 : a white piece
 * - 2 : a black piece
 */
inline unsigned int get_value(const T_Board& board, unsigned int p)
{
	return ((board[0] >> p) & MASK) * (((board[1] >> p) & MASK) + 1); // Get value
}

/** 
 * @brief move moves a piece from \a src to a new coordinate \a dest. The \a board
 * will be updated.
 * @param board is the board of the game
 * @param src is the dark square source number
 * @param dest is the dark square destination number
 */
inline void move(T_Board& board, unsigned int src, unsigned int dest)
{
	assert((board[0] >> src) & MASK); // Ensure src square is not empty
	T_UINT value = (board[1] >> src) & MASK; // Get value
	board[0] &= ~(MASK << src); // Clear src square (first uint)
	board[0] |= MASK << dest; // Update dest square (first uint)
	board[1] &= ~(MASK << dest); // Update dest square (first uint)
	board[1] |= value << dest; // Update dest square (second uint)
}

/** 
 * @brief remove removes a piece \a p from the board. The \a board will be updated.
 * @param board is the board of the game
 * @param p is the dark square number
 */
inline void remove(T_Board& board, unsigned int p)
{
	board[0] &= ~(MASK << p); // Clear square
}

/** 
 * @brief board_count returns the number of pieces of COLOR in \a src
 * @param src is the source board
 */
template< unsigned int COLOR > unsigned int board_count(const T_Board& src)
{
	unsigned int nb = 0;
	for (unsigned int i=0; i < sizeof(T_UINT)*8; ++i)
	{
		auto v = (src[0] >> i) & MASK; // Get status of square (free or occupied)
		if (v)
		{
			auto c = ((src[1] >> i) & MASK) + 1; // Get color of piece
			if (c == COLOR) ++nb;
		}
	}
	return nb;
}

/** 
 * @brief board_diff_removed returns the pieces of COLOR that have been removed from \a src
 * @param src is the source board
 * @param dest is the second board (to make the diff)
 */
template< unsigned int COLOR > std::vector< unsigned int > board_diff_removed(const T_Board& src, const T_Board& dest)
{
	std::vector< unsigned int > res;
	for (unsigned int i=0; i < sizeof(T_UINT)*8;  ++i)
	{
		auto v1 = (src[0] >> i) & MASK; // Get status of src square (free or occupied)
		auto v2 = (dest[0] >> i) & MASK; // Get status of dest square (free or occupied)
		auto c1 = ((src[1] >> i) & MASK) + 1; // Get color of src piece
		auto c2 = ((dest[1] >> i) & MASK) + 1; // Get color of dest piece
		if (   ((v1 == 1) && (v2 == 0) && (c1 == COLOR)) 
			|| ((v1 == 1) && (v2 == 1) && (c2 != COLOR) && (c1 == COLOR)) )
			res.push_back(i);
	}
	return res;
}

/** 
 * @brief board_diff_added returns the pieces of COLOR that have been added to \a src
 * @param src is the source board
 * @param dest is the second board (to make the diff)
 */
template< unsigned int COLOR > std::vector< unsigned int > board_diff_added(const T_Board& src, const T_Board& dest)
{
	std::vector< unsigned int > res;
	for (unsigned int i=0; i < sizeof(T_UINT)*8;  ++i)
	{
		auto v1 = (src[0] >> i) & MASK; // Get status of src square (free or occupied)
		auto v2 = (dest[0] >> i) & MASK; // Get status of dest square (free or occupied)
		auto c1 = ((src[1] >> i) & MASK) + 1; // Get color of src piece
		auto c2 = ((dest[1] >> i) & MASK) + 1; // Get color of dest piece
		if (   ((v1 == 0) && (v2 == 1) && (c2 == COLOR)) 
			|| ((v1 == 1) && (v2 == 1) && (c1 != COLOR) && (c2 == COLOR)) )
			res.push_back(i);
	}
	return res;
}

/**
 * @brief Create a board with pieces at starting positions
 * @param board is the real board to use
 * @param width is the number of squares on a single line
 * @param height is the number of squares on a single column
 */
inline void create_board(T_Board& board, unsigned int width, unsigned int height)
{
	unsigned int nb_pieces = (height<=4?1:2) * width;
	unsigned int size = width*height;
	board[0] = board[1] = 0;
	for (unsigned int i=0; i < nb_pieces; ++i)
	{
		unsigned int j = size - i - 1;
		// White piece
		board[0] |= MASK << j; // Update square (first uint)
		// Black piece
		board[0] |= MASK << i; // Update square (first uint)
		board[1] |= MASK << i; // Update square (second uint)
	}
}

/**
 * @brief Load the board stored in the file \a file_name.
 * @param file_name is the name of the file
 * @param board is the real board to use
 * @param width is the number of squares on a single line
 * @param height is the number of squares on a single column
 */
inline void load_from_file(std::string file_name, T_Board& board, unsigned int& width, unsigned int& height)
{
	unsigned int size = 0;
	width = 0;
	height = 0;
	for (unsigned int i=0; i < board.size(); ++i)
		board[i] = 0;
	std::ifstream in_f(file_name.c_str());
	if (in_f.is_open())
	{
		unsigned char c;
		std::string l;
		while (std::getline(in_f, l))
		{
			std::stringstream in_s(l);
			while (in_s >> c)
			{
				if ((c >= '1') && (c <= '2'))
				{
					board[0] |= MASK << size; // Update square (first uint)
					T_UINT value = c - '0' - 1;
					assert(value == 0 || value == 1);
					board[1] |= (value << size); // Update square (second uint)
				}
				++size;
			}
			if (height == 0) width = size;
			assert((size / width) == (height + 1));
			++height;
		}
		assert(size == width*height);
	} else {
		std::cerr << "Can't open file '" << file_name << "'" << std::endl;
	}
}

/**
 * @brief print the board
 * @param board is the board to use
 * @param width is the number of squares on a single line
 * @param height is the number of squares on a single column
 * @param os the output stream
 */
inline void print_board(const T_Board& board, unsigned int width, unsigned int height, std::ostream& os)
{
	for (unsigned int i=0; i < height; ++i)
	{
		for (unsigned int j=0; j < width; ++j)
			os << get_value(board,i*width+j);
		os << std::endl;
	}
}

/**
 * @Brief Board_wrapper is a wrapper to a T_Board for Breakthrough
 * Notice that a Board_wrapper instance is taken a T_Board element as material. Is is done this way
 * in order to decrease the number of bits required to store a lot of boards when we already
 * known the width and the height of a board.
 */
template< const unsigned int COLOR >
class Board_wrapper
{
private:
	T_Board _board;			///< Reference to the real board
	std::uint8_t _width;	///< width of the board
	std::uint8_t _height;	///< height of the board

public:
	/**
	 * @brief Constructor creates a board from an existing board
	 * @param board is the real board to use
	 * @param width is the number of squares on a single line
	 * @param height is the number of squares on a single column
	 */
	inline Board_wrapper(T_Board board, unsigned int width, unsigned int height)
		: _board(board), _width(width), _height(height)
	{ }

	/**
	 * @brief Iterator on a board for a given player (WHITE or BLACK)
	 */
	class iterator {
		static constexpr unsigned int INV_COLOR = 1 - (COLOR - 1) + 1;
		static constexpr uint8_t END_BOARD = 99; // 999 stands for no piece found (end of board)
		friend class Board_wrapper; // Friend statement
	public:
		/**
		 * Check if current iterator reached the end of the board.
		 * @return True if no move is possible, false otherwise
		 */
		bool at_end()
		{
			return _move.square == END_BOARD;
		}

		/**
		 * Check if move pointed by current iterator is a winning move
		 * @return True if current move is a winning move, false otherwise
		 */
		bool winning_move()
		{
			return _winning_move;
		}

		/**
		 * BE CAREFUL, this operator is not a "==" operator, it only checks if
		 * current iterator reached the end of the board.
		 * @return True if no move is possible, false otherwise
		 */
		bool operator==(const iterator& )
		{
			return _move.square == END_BOARD;
		}

		/**
		 * BE CAREFUL, this operator is not a "!=" operator, it only checks if
		 * current iterator reached the end of the board.
		 * @return False if no move is possible, true otherwise
		 */
		bool operator!=(const iterator& )
		{
			return _move.square != END_BOARD;
		}

		/**
		 * Return value under current move
		 * @return A board with the current move applied
		 */
		std::pair<T_Board, bool> operator*()
		{
			return std::make_pair(next_board(), winning_move());
		}

		/**
		 * Move the iterator to the next move.
		 * @return A reference to the current iterator
		 */
		iterator& operator++()
		{
			++_move.dir;
			search_next_move();
			return *this;
		}

	private:
		T_Board _board;				///< The board
		std::uint8_t _end;			///< End of board (first impossible square)
		std::uint8_t _width;		///< Number of squares on a line
		std::uint8_t _height;		///< Number of squares on a column
		struct {
			std::uint8_t square;
			std::uint8_t dir;
		} _move;					///< Current move on this board (Num_square, direction of current move (0,1,2,3) 3 means no direction for move) 
		bool _winning_move;			///< True if current move is a winning move

		/**
		 * Default constructor
		 * @param board the board to take in input
		 * @param width is the width of the board
		 * @param height is the height of the board
		 */
		iterator(T_Board board, unsigned int width, unsigned int height)
			: _board(board), _end(width*height), _width(width), _height(height), _move({END_BOARD, 3}), _winning_move(false)
		{
			// Search for the first square of the good color
			assert(_end != 0);
			std::uint8_t i=0;
			auto p = get_value(_board,i);
			while (p != COLOR)
			{
				++i;
				if (i == _end) break; // End of board
				p = get_value(_board,i);
			}
			// We find a piece on the board
			if (i != _end)
			{
				_move = {i, 0};
				search_next_move();
			}
		}

		/**
		 * Constructor on a empty move (end iterator)
		 * @param board the board to take in input
		 */
		iterator(const T_Board& board)
			: _board(board), _end(0), _width(0), _height(0), _move({END_BOARD,3})
		{ }

		/**
		 * Return the new board if current move is applied
		 * @return A new board with move done
		 */
		T_Board next_board() const
		{
			assert(_move.square != END_BOARD);
			assert(_move.dir < 3);
			T_Board new_board = _board;
			std::uint8_t dest_square = (COLOR==WHITE)?(_move.square-_width+_move.dir-1):(_move.square+_width+_move.dir-1);
			move(new_board, _move.square, dest_square);
			return new_board;
		}

		/**
		 * Search for next possible move according to checkers rules
		 */
		void search_next_move()
		{
			assert(_move.square != END_BOARD);
			unsigned int WIDTH_color = ((COLOR==WHITE)?-_width:_width);
			_winning_move = false;

			while (true)
			{
				// Verifying that square color matches COLOR
				assert((((_board[1] >> _move.square) & MASK) + 1) == COLOR);

				auto line = _move.square / _width;
				auto col = _move.square % _width;

				// We can't move outside board
				if ((COLOR == WHITE) && (line == 0)) _move.dir = 3;
				if ((COLOR == BLACK) && (line == _height-1)) _move.dir = 3;

				switch (_move.dir) // check in all possible directions
				{
					case 0:
						if (col > 0)
						{
							std::uint8_t n_square = _move.square + WIDTH_color - 1;
							if (get_value(_board,n_square) != COLOR)
								break;
						}
						_move.dir = 1;
						[[fallthrough]];
					case 1:
						if (   ((COLOR == BLACK) && (line < (_height - 1)))
								|| ((COLOR == WHITE) && (line > 0)) )
						{
							std::uint8_t n_square = _move.square + WIDTH_color;
							if (get_value(_board,n_square) == 0)
								break;
						}
						_move.dir = 2;
						[[fallthrough]];
					case 2:
						if (col < (_width - 1))
						{
							std::uint8_t n_square = _move.square + WIDTH_color + 1;
							if (get_value(_board,n_square) != COLOR)
								break;
						}
						_move.dir = 3;
				}

				if (_move.dir != 3)
				{
					// A move has been found we return
					if ((COLOR == WHITE) && (line == 1)) _winning_move = true;
					if ((COLOR == BLACK) && (line == (_height-2))) _winning_move = true;
					return;
				}

				// Search for the next square of the good color
				++_move.square;
				if (_move.square == _end) { _move = {END_BOARD, 3}; return; } // End of board
				auto p = get_value(_board,_move.square);
				while (p != COLOR)
				{
					++_move.square;
					if (_move.square == _end) { _move = {END_BOARD, 3}; return; } // End of board
					p = get_value(_board,_move.square);
				}
				_move.dir = 0;
			}
		}
	};

	/**
	 * @brief Return an iterator on the first move of the board
	 * @return An iterator on the first move of the board
	 */
	iterator begin()
	{
		return iterator(_board,_width,_height);
	}

	/**
	 * @brief Return an iterator on the end of the board, i.e. with an empty move
	 * @return An iterator to the end of the board
	 */
	iterator end()
	{
		return iterator(_board);
	}

};

