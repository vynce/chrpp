/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <iostream>
#include <string>
#include <chrpp.hh>

/**
 * @brief Fibnoacci Heap
 *
 * Implementation of the Fibonacci heap data structure with optimal time complexity.
 * This handler is used by the Dijkstra handler, but can just as well be used 
 * elsewhere.
 * \ingroup Examples
 *
	<CHR name="FIB_HEAP">
		<chr_constraint> extract_min(?int,?int),
						insert(+int,+int), decr_or_ins(+int,+int)

		<chr_constraint> mark(+int), decr(+int,+int), ch2rt(+int),
						decr5(+int,+int,+int,+int,+bool), 
						item(+int,+int,+int,+int,+bool), 
						min(+int,+int), findmin()
		
		insert @ insert(I,K) <=> item(I,K,0,0,false), min(I,K);;
		
		keep_min @ min(_,A) \ min(_,B) <=> A <= B | success();;
		
		extr        @ extract_min(X,Y), min(I,K), item(I,_,_,_,_)
					<=> ch2rt(I), findmin(), X%=I, Y%=K;;
		extr_empty  @ extract_min(X,Y) <=> X%=0, Y%=0;;
		
		c2r      @ ch2rt(I) \ item(C,K,R,I,_)# passive <=> item(C,K,R,0,false);;
		c2r_done @ ch2rt(_) <=> success();;
		
		find_min  @ findmin(), item(I,K,_,0,_) ==> min(I,K);;
		found_min @ findmin() <=> success();;
		
		same_rank @ item(I1,K1,R,0,_), item(I2,K2,R,0,_)
	        <=> K1 <= K2 | item(I2,K2,R,I1,false), item(I1,K1,R+1,0,false);;
		
		decr     @ decr(I,K), item(I,O,R,P,M) <=> K < O | decr5(I,K,R,P,M);;
		decr_nok @ decr(_,_) <=> success();;
		
		doi_decrease @ item(I, O, R, P, M),  decr_or_ins(I,K) <=> K < O  | decr5(I,K,R,P,M);;
		doi_nop      @ item(I, O, _, _, _) \ decr_or_ins(I,K) <=> K >=O  | success();;
		doi_insert   @ decr_or_ins(I,K) <=> insert(I,K);;

		d_min  @ decr5(I, K,_,_,_) ==> min(I,K);;
		d_root @ decr5(I, K, R, 0,_) <=> item(I,K,R,0,false);;
		d_ok   @ item(P, PK,_,_,_) \ decr5(I,K,R,P,M) <=> K >= PK | item(I,K,R,P,M);;
		d_prob @ decr5(I,K,R,P,_) <=> item(I,K,R,0,false), mark(P);;

		mark_root     @ mark(I), item(I,K,R,0,_)  <=> item(I,K,R-1,0,false);;
		mark_marked   @ mark(I), item(I,K,R,P,true) <=> item(I,K,R-1,0,false), mark(P);;
		mark_unmarked @ mark(I), item(I,K,R,P,false) <=> item(I,K,R-1,P,true);;
		error_mark    @ mark(_) <=> failure();;
	</CHR>
 */

/**
 * @brief Dijkstra's shortest path algorithm
 *
 * Implementation of Dijkstra's classical shortest path algorithm.
 * \ingroup Examples
 *
	<CHR name="DIJKSTRA" parameters="FIB_HEAP& fh">
		<chr_constraint> edge(?int,?int,?int), dijkstra(?int), scan(?int,?int),
						relabel(?int,?int), distance(?int,?int)

		start_scanning @ dijkstra(A) <=> scan(A,0);;

		stop_scanning @ scan(0,_) <=> success();;
		label_neighb  @ scan(N,L), edge(N,N2,W) ==> relabel(N2, L + W);;
		scan_next @ scan(N,L) <=> distance(N,L), fh.extract_min(QN,QW), scan(QN,QW);;

		scanned 	@ distance(N,_) \ relabel(N,_) <=> success();;
		not_scanned	@ relabel(N,L) <=> fh.decr_or_ins(N,L);;
	</CHR>
 */

template < typename T >
void print(T& pb)
{
	auto it = pb.chr_store_begin();
	while (!it.at_end())
	{
		std::cout << it.to_string() << std::endl;
		++it;
	}
}

int main()
{
	TRACE( chr::Log::register_flags(chr::Log::ALL); )

	auto fh = FIB_HEAP::create();
	auto space = DIJKSTRA::create(*fh);
	CHR_RUN( space->edge(2,3,1); )
	print(*space);
	chr::Statistics::print(std::cout);
	return 0;
}
