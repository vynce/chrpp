/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <array>
#include <vector>
#include <unordered_set>
#include <fstream>
#include <sstream>
#include <cassert>

/** 
 * Board definition. Each square of the board is numbered and stores a piece.
 * A piece may be a man or a flying king.
 * On a NxN board, only N/2 squares are meaningfull (the dark ones), each square can take
 * five values :
 * - 0 : the square is free
 * - 1 : a light man
 * - 2 : a dark man
 * - 3 : a light flying king
 * - 4 : a dark flying king
 * We need 3 bits to store a square data. As a result, to store a 10x10 board size, we need
 * 3 * 50 = 150 bits.
 * We can tackle these board sizes :
 * 10 : [0 - 49] --> 3bits * 50 = 150bits
 *  9 : [0 - 39] --> 3bits * 40 = 120bits
 *  8 : [0 - 31] --> 3bits * 32 = 150bits
 *  7 : [0 - 23] --> 3bits * 24 = 72bits
 *  6 : [0 - 17] --> 3bits * 18 = 54bits
 *  4 : [0 - 7] --> 3bits * 8 = 24bits
 *  3 : [0 - 3] --> 3bits * 4 = 12bits
 *  As we used std::uint64_t element to store data, only the 63 first bits are relevant the
 *  last one is unused. It's because we can store only 21 squares (3*21 = 63bits) on a std::uint64_t
 *  integer. As a result, we can store 63 dark slots on a T_board element 
 *
 */
using T_UINT = std::uint64_t;
using T_Board = std::array< T_UINT, 3 >; // We limit to boards of 100 dark squares
const unsigned int SQUARE_NB_BITS = 3; ///< Number of bits needed to encode a square number
const unsigned int NB_BITS_BY_ARRAY_SLOT = sizeof(T_UINT) * 8 - 1; ///< Number of bits in a slot of the array
const unsigned int MAX_NB_SQUARES = NB_BITS_BY_ARRAY_SLOT / SQUARE_NB_BITS * 3; ///< Maximum number of square which can be stored on a T_Board array
const uint64_t MASK3 = 7u; ///< Mask for extracting a square number from a T_Board element

const unsigned int LIGHT = 1; ///< Light player
const unsigned int DARK  = 2; ///< Dark player

/** 
 * @brief get_value returns the value of the wanted dark square \a n of \a board
 * @param board is the board of the game
 * @param p is the number of the dark square to check
 * @return the value of the wanted dark square:
 * - 0 : the square is free
 * - 1 : a dark man
 * - 2 : a light man
 * - 3 : a dark flying king
 * - 4 : a light flying king
 */
inline unsigned int get_value(const T_Board& board, unsigned int p)
{
	return (board[(p * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] >> ((p * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)) & MASK3; // Get value
}

/** 
 * @brief move moves a piece from \a src to a new coordinate \a dest. The \a board
 * will be updated.
 * @param board is the board of the game
 * @param src is the dark square source number
 * @param dest is the dark square destination number
 */
inline void move(T_Board& board, unsigned int src, unsigned int dest)
{
	assert(((board[(src * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] >> ((src * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)) & MASK3) != 0); // Ensure src square is not empty
	assert(((board[(dest * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] >> ((dest * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)) & MASK3) == 0); // Ensure dest square is empty
	T_UINT value = (board[(src * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] >> ((src * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)) & MASK3; // Get value
	board[(src * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] &= ~(MASK3 << ((src * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)); // Clear src square
	board[(dest * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] |= value << ((dest * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT); // Update dest square
}

/** 
 * @brief remove removes a piece \a p from the board. The \a board will be updated.
 * @param board is the board of the game
 * @param p is the dark square number
 */
inline void remove(T_Board& board, unsigned int p)
{
	board[(p * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] &= ~(MASK3 << ((p * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)); // Clear src square
}

/** 
 * @brief crown crowns a man piece \a p to a flying king. The \a board will be updated.
 * @param board is the board of the game
 * @param p is the dark square number
 */
inline void crown(T_Board& board, unsigned int p)
{
	assert(((board[(p * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] >> ((p * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)) & MASK3) != 0); // Ensure p is not empty
	assert(((board[(p * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] >> ((p * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)) & MASK3) < 3); // Ensure p is a man
	T_UINT value = 2u + ((board[(p * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] >> ((p * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)) & MASK3); // Get value
	board[(p * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] &= ~(MASK3 << ((p * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)); // Clear square
	board[(p * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] |= value << ((p * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT); // Update square
}

/** 
 * @brief board_count returns the number of pieces of COLOR in \a src
 * @param src is the source board
 */
template< unsigned int COLOR > unsigned int board_count(const T_Board& src)
{
	unsigned int nb = 0;
	for (unsigned int i=0; i < src.size(); ++i)
	{
		for (unsigned int j=0; j < NB_BITS_BY_ARRAY_SLOT / SQUARE_NB_BITS; ++j)
		{
			auto v = (src[i] >> ((j * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)) & MASK3; // Get value
			if ((v == COLOR) || (v == COLOR+2)) ++nb;
		}
	}
	return nb;
}

/** 
 * @brief board_diff_removed returns the pieces of COLOR that have been removed from \a src
 * @param src is the source board
 * @param dest is the second board (to make the diff)
 */
template< unsigned int COLOR > std::vector< unsigned int > board_diff_removed(const T_Board& src, const T_Board& dest)
{
	std::vector< unsigned int > res;
	unsigned int k = 0;
	for (unsigned int i=0; i < src.size(); ++i)
	{
		if (src[i] != dest[i])
		{
			for (unsigned int j=0; j < NB_BITS_BY_ARRAY_SLOT / SQUARE_NB_BITS; ++j, ++k)
			{
				auto v1 = (src[i] >> ((j * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)) & MASK3; // Get value
				auto v2 = (dest[i] >> ((j * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)) & MASK3; // Get value
				if (((v1 == COLOR) || (v1 == COLOR+2)) && (v2 == 0))
					res.push_back(k);
			}
		} else
			k += MAX_NB_SQUARES;
	}
	return res;
}

/** 
 * @brief board_diff_added returns the pieces of COLOR that have been added to \a src
 * @param src is the source board
 * @param dest is the second board (to make the diff)
 */
template< unsigned int COLOR > std::vector< unsigned int > board_diff_added(const T_Board& src, const T_Board& dest)
{
	std::vector< unsigned int > res;
	unsigned int k = 0;
	for (unsigned int i=0; i < src.size(); ++i)
	{
		if (src[i] != dest[i])
		{
			for (unsigned int j=0; j < NB_BITS_BY_ARRAY_SLOT / SQUARE_NB_BITS; ++j, ++k)
			{
				auto v1 = (src[i] >> ((j * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)) & MASK3; // Get value
				auto v2 = (dest[i] >> ((j * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)) & MASK3; // Get value
				if (((v2 == COLOR) || (v2 == COLOR+2)) && (v1 == 0))
					res.push_back(k);
			}
		} else
			k += MAX_NB_SQUARES;
	}
	return res;
}

/**
 * @brief Create a board with pieces at starting positions
 * @param board is the real board to use
 * @param width is the number of squares on a single line
 * @param height is the number of squares on a single column
 */
inline void create_board(T_Board& board, unsigned int width, unsigned int height)
{
	unsigned int nb_pieces = std::max(1u,(height - 2) / 2) * width / 2;
	unsigned int size = width*height/2;
	for (unsigned int i=0; i < board.size(); ++i)
		board[i] = 0;
	for (unsigned int i=0; i < nb_pieces; ++i)
	{
		unsigned int j = size - i - 1;
		board[(i * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] |= (static_cast<T_UINT>(LIGHT) << ((i * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)); // Update square with dark piece
		board[(j * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] |= (static_cast<T_UINT>(DARK) << ((j * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)); // Update square with light piece
	}
}

/**
 * @brief Load the board stored in the file \a file_name.
 * @param file_name is the name of the file
 * @param board is the real board to use
 * @param width is the number of squares on a single line
 * @param height is the number of squares on a single column
 */
inline void load_from_file(std::string file_name, T_Board& board, unsigned int& width, unsigned int& height)
{
	unsigned int size = 0;
	unsigned int nb_dark_slots = 0;
	bool dark_slot_read = false;
	width = 0;
	height = 0;
	for (unsigned int i=0; i < board.size(); ++i)
		board[i] = 0;
	std::ifstream in_f(file_name.c_str());
	if (in_f.is_open())
	{
		unsigned char c;
		std::string l;
		while (std::getline(in_f, l))
		{
			dark_slot_read = false;
			std::stringstream in_s(l);
			while (in_s >> c)
			{
				if ((c >= '0') && (c <= '4'))
				{
					T_UINT value = c - '0';
					board[(size * SQUARE_NB_BITS) / NB_BITS_BY_ARRAY_SLOT] |= (value << ((size * SQUARE_NB_BITS) % NB_BITS_BY_ARRAY_SLOT)); // Update square
					++size;
					++nb_dark_slots;
					dark_slot_read = true;
				}
			}
			if (dark_slot_read)
			{
				++height;
				if (height == 2) width = nb_dark_slots;
			}
		}
		assert((2*size + (width*height) % 2) == width*height);
	} else {
		std::cerr << "Can't open file '" << file_name << "'" << std::endl;
	}
}

/**
 * @brief print the board
 * @param board is the board to use
 * @param width is the number of squares on a single line
 * @param height is the number of squares on a single column
 * @param os the output stream
 */
inline void print_board(const T_Board& board, unsigned int width, unsigned int height, std::ostream& os)
{
	unsigned int idx = 0;
	for (unsigned int i=0; i < height; ++i)
	{
		for (unsigned int j=0; j < width; ++j)
		{
			if ((i + j) % 2 == 0) os << ' ';
			else os << get_value(board,idx++);
		}
		os << std::endl;
	}
}

/**
 * @Brief Board_wrapper is a wrapper to a T_Board for checkers
 * Notice that a Board_wrapper instance is taken a T_Board element as material. Is is done this way
 * in order to decrease the number of bits required to store a lot of boards when we already
 * known the width and the height of a board.
 */
template< const unsigned int COLOR >
class Board_wrapper
{
private:
	T_Board _board;			///< Reference to the real board
	std::uint8_t _width;	///< width of the board
	std::uint8_t _height;	///< height of the board

public:
	/**
	 * @brief Constructor creates a board from an existing board
	 * @param board is the real board to use
	 * @param width is the number of squares on a single line
	 * @param height is the number of squares on a single column
	 */
	inline Board_wrapper(T_Board board, unsigned int width, unsigned int height)
		: _board(board), _width(width), _height(height)
	{ }

	/**
	 * @brief Iterator on a board for a given player (LIGHT or DARK)
	 */
	class iterator {
		static constexpr unsigned int INV_COLOR = 1 - (COLOR - 1) + 1;
		friend class Board_wrapper; // Friend statement
	public:
		/**
		 * Return the starting position of move
		 * @return the starting position of move
		 */
		unsigned int get_start_pos() const
		{
			return _move.front().first;
		}

		/**
		 * Return the ending position of move
		 * @return the ending position of move
		 */
		unsigned int get_end_pos() const
		{
			return _move.back().first;
		}

		/**
		 * Return the length of current move
		 * @return the length of current_move
		 */
		unsigned int move_length() const
		{
			return _jumped.size();
		}

		/**
		 * Check if current iterator reached the end of the board.
		 * @return True if no move is possible, false otherwise
		 */
		bool at_end()
		{
			return _move.empty();
		}

		/**
		 * BE CAREFUL, this operator is not a "==" operator, it only checks if
		 * current iterator reached the end of the board.
		 * @return True if no move is possible, false otherwise
		 */
		bool operator==(const iterator& )
		{
			return _move.empty();
		}

		/**
		 * BE CAREFUL, this operator is not a "!=" operator, it only checks if
		 * current iterator reached the end of the board.
		 * @return False if no move is possible, true otherwise
		 */
		bool operator!=(const iterator& )
		{
			return !_move.empty();
		}

		/**
		 * Return value under current move
		 * @return A board with the current move applied
		 */
		T_Board operator*()
		{
			return next_board();
		}

		/**
		 * Move the iterator to the next move.
		 * @return A reference to the current iterator
		 */
		iterator& operator++()
		{
			search_next_move();
			return *this;
		}

	private:
		struct quator {
			std::uint8_t first;
			std::uint8_t second;
			std::uint8_t third;
			std::uint8_t fourth;
		};
		T_Board _board;								///< The board
		std::uint8_t _end;							///< End of board (first impossible square)
		std::uint8_t _width;						///< Number of squares on a line
		std::uint8_t _height;						///< Number of squares on a column
		std::uint8_t _dark_width;					///< Number of dark squares on a single line (an even line number)
		unsigned int _move_length;					///< Minimum length required for a move 
		std::vector< quator > _move;				///< Current move on this board. Stack of (Num_landed_dark_square, direction of next move to try, jumped piece, direction of initial jump) 
		std::unordered_set< std::uint8_t > _jumped;	///< Current set of jumped pieces

		/**
		 * Default constructor
		 * @param board the board to take in input
		 * @param width is the width of the board
		 * @param height is the height of the board
		 */
		iterator(T_Board board, unsigned int width, unsigned int height)
			: _board(board), _end(width*height/2), _width(width), _height(height), _dark_width(width/2), _move_length(0u)
		{
			// We can only tackle boards of even width size
			assert((width % 2) == 0);
			// Search for the first square of the good color
			assert(_end != 0);
			std::uint8_t i=0;
			auto p = get_value(_board,i);
			while ((p != COLOR) && (p != (COLOR+2)))
			{
				++i;
				if (i == _end) break; // End of board
				p = get_value(_board,i);
			}
			// We find a piece on the board
			if (i != _end)
			{
				_move.push_back({i, 0, _end, _end});
				// Search for maximum move length
				std::vector< quator > cur_move = _move;
				std::unordered_set< std::uint8_t > cur_jumped;
				unsigned int cur_move_length = 0;
				while (! at_end())
				{
					search_next_move();
					if ((cur_move_length < _move_length) || (cur_move.size() == 1))
					{
						cur_move_length = _move_length;
						cur_move = _move;
						cur_jumped = _jumped;
					}
				}
				// We return to the first move of maximum length
				_move = cur_move;
				_jumped = cur_jumped;
			}
		}

		/**
		 * Constructor on a empty move (end iterator)
		 * @param board the board to take in input
		 */
		iterator(const T_Board& board)
			: _board(board), _end(0), _width(0), _height(0), _dark_width(0), _move_length(0)
		{ }

		/**
		 * Return the new board if current move is applied
		 * @return A new board with move done
		 */
		T_Board next_board() const
		{
			T_Board new_board = _board;
			for (auto p : _jumped) remove(new_board, p);
			if (_move.front().first !=  _move.back().first)
			{
				auto dest_square = _move.back().first;
				move(new_board, _move.front().first, dest_square);

				// A move has been found we return
				auto line = 2 * dest_square / _width;
				// We crown the last piece if needed
				if ((get_value(new_board,dest_square) == LIGHT) && (line == (_height - 1))) crown(new_board,dest_square);
				if ((get_value(new_board,dest_square) == DARK) && (line == 0)) crown(new_board,dest_square);
			}

			return new_board;
		}

		/**
		 * Search for next possible jump move for a flying king piece
		 * @param starting_square is the starting square for this search of next move
		 */
		void search_next_jump_flying_move(std::uint8_t starting_square)
		{
			assert(!_move.empty());
			std::uint8_t n_square = 255u; // Next landed square
			std::uint8_t n_square_j = 255u; // Jumped piece
			std::uint8_t line, line2, col, col2, even_line, even_line2;
			unsigned int value;
			unsigned int cur_move_length = _move.size() - 1;
			bool move_done = false;

			while (!_move.empty())
			{
				auto& square = _move.back().first;
				auto& dir = _move.back().second;
				auto jump_dir = _end;
				line = 2 * square / _width;
				even_line = line % 2;
				col = (2 * square) % _width + (1 - even_line);

				switch (dir) // Check move in every possible direction
				{
					case 0:
						dir = 1; // Update dir for next search
						if ((line > 1) && (col > 1))
						{
							n_square_j = square - _dark_width - even_line;
							value = get_value(_board,n_square_j);
							line2 = line - 1; col2 = col - 1; even_line2 = 1 - even_line;
							while ((value == 0) && (line2 > 1) && (col2 > 1))
							{
								n_square_j = n_square_j - _dark_width - even_line2;
								value = get_value(_board,n_square_j);
								--line2; --col2; even_line2 = 1 - even_line2;
							}
							if (((value == INV_COLOR) || (value == (INV_COLOR+2))) && (_jumped.find(n_square_j) == _jumped.end()))
							{
								n_square = n_square_j - _dark_width - even_line2;
								if ((n_square == starting_square) || (get_value(_board,n_square) == 0))
								{
									jump_dir=0;
									break;
								}
							}
						}
						[[fallthrough]];
					case 1:
						dir = 2; // Update dir for next search
						if ((line > 1) && (col < (_width - 2)))
						{
							n_square_j = square - _dark_width + 1 - even_line;
							value = get_value(_board,n_square_j);
							line2 = line - 1; col2 = col + 1; even_line2 = 1 - even_line;
							while ((value == 0) && (line2 > 1) && (col2 < (_width - 2)))
							{
								n_square_j = n_square_j - _dark_width + 1 - even_line2;
								value = get_value(_board,n_square_j);
								--line2; ++col2; even_line2 = 1 - even_line2;
							}
							if (((value == INV_COLOR) || (value == (INV_COLOR+2))) && (_jumped.find(n_square_j) == _jumped.end()))
							{
								n_square = n_square_j - _dark_width + 1 - even_line2;
								if ((n_square == starting_square) || (get_value(_board,n_square) == 0))
								{
									jump_dir=1;
									break;
								}
							}
						}
						[[fallthrough]];
					case 2:
						dir = 3; // Update dir for next search
						if ((line < (_height - 2)) && (col < (_width - 2)))
						{
							n_square_j = square + _dark_width + 1 - even_line;
							value = get_value(_board,n_square_j);
							line2 = line + 1; col2 = col + 1; even_line2 = 1 - even_line;
							while ((value == 0) && (line2 < (_height - 2)) && (col2 < (_width - 2)))
							{
								n_square_j = n_square_j + _dark_width + 1 - even_line2;
								value = get_value(_board,n_square_j);
								++line2; ++col2; even_line2 = 1 - even_line2;
							}
							if (((value == INV_COLOR) || (value == (INV_COLOR+2))) && (_jumped.find(n_square_j) == _jumped.end()))
							{
								n_square = n_square_j + _dark_width + 1 - even_line2;
								if ((n_square == starting_square) || (get_value(_board,n_square) == 0))
								{
									jump_dir=2;
									break;
								}
							}
						}
						[[fallthrough]];
					case 3:
						dir = 4; // Update dir for searching the landed square
						if ((line < (_height - 2)) && (col > 1))
						{
							n_square_j = square + _dark_width - even_line;
							value = get_value(_board,n_square_j);
							line2 = line + 1; col2 = col - 1; even_line2 = 1 - even_line;
							while ((value == 0) && (line2 < (_height - 2)) && (col2 > 1))
							{
								n_square_j = n_square_j + _dark_width - even_line2;
								value = get_value(_board,n_square_j);
								++line2; --col2; even_line2 = 1 - even_line2;
							}
							if (((value == INV_COLOR) || (value == (INV_COLOR+2))) && (_jumped.find(n_square_j) == _jumped.end()))
							{
								n_square = n_square_j + _dark_width - even_line2;
								if ((n_square == starting_square) || (get_value(_board,n_square) == 0))
								{
									jump_dir=3;
									break;
								}
							}
						}
						[[fallthrough]];
					case 4:
						// No possible move from this square.
						// If we moved more than the required length, then the move is valid and we return.
						// Otherwise :
						// 1 - We check for another landed square,
						// 2 - If no such square, we close the square and go back to the previous one.
						if (move_done && (cur_move_length >= _move_length))
						{
							_move_length = cur_move_length;
							return;
						}
						{
							switch (_move.back().fourth) // Check next landed square
							{
								case 0:
									if ((line > 0) && (col > 0))
									{
										n_square = square - _dark_width - even_line;
										if (get_value(_board,n_square) == 0)
										{
											dir = 0;
											square = n_square;
											move_done = true;
											continue;
										}
									}
									break;
								case 1:
									if ((line > 0) && (col < (_width - 1)))
									{
										n_square = square - _dark_width + 1 - even_line;
										if (get_value(_board,n_square) == 0)
										{
											dir = 0;
											square = n_square;
											move_done = true;
											continue;
										}
									}
									break;
								case 2:
									if ((line < (_height - 1)) && (col < (_width - 1)))
									{
										n_square = square + _dark_width + 1 - even_line;
										if (get_value(_board,n_square) == 0)
										{
											dir = 0;
											square = n_square;
											move_done = true;
											continue;
										}
									}
									break;
								case 3:
									if ((line < (_height - 1)) && (col > 0))
									{
										n_square = square + _dark_width - even_line;
										if (get_value(_board,n_square) == 0)
										{
											dir = 0;
											square = n_square;
											move_done = true;
											continue;
										}
									}
							}
						}
						_jumped.erase(_move.back().third);
						_move.pop_back();
						--cur_move_length;
						continue; // Backtrack to previous unclosed square
				}
				assert(cur_move_length <= _width * _height); // Just to be sure that cur_move_length is well formed because of first initial value
				assert(n_square != 255u);
				assert(n_square_j != 255u);
				_jumped.insert(n_square_j);
				assert((jump_dir >= 0) and (jump_dir <= 3));
				_move.push_back({n_square, 0, n_square_j, jump_dir});
				++cur_move_length;
				move_done = true;
			}
		}


		/**
		 * Search for next possible jump move for a normal piece
		 * @param starting_square is the starting square for this search of next move
		 */
		void search_next_jump_piece_move(std::uint8_t starting_square)
		{
			assert(!_move.empty());
			std::uint8_t n_square = 255u; // Next landed square
			std::uint8_t n_square_j = 255u; // Jumped piece
			std::uint8_t line, col, even_line;
			unsigned int value;
			unsigned int cur_move_length = _move.size() - 1;
			bool move_done = false;

			while (!_move.empty())
			{
				auto square = _move.back().first;
				auto& dir = _move.back().second;
				line = 2 * square / _width;
				even_line = line % 2;
				col = (2 * square) % _width + (1 - even_line);

				switch (dir) // Check move in every possible direction
				{
					case 0:
						dir = 1; // Update dir for next search
						if ((line > 1) && (col > 1))
						{
							n_square_j = square - _dark_width - even_line;
							value = get_value(_board,n_square_j);
							if (((value == INV_COLOR) || (value == (INV_COLOR+2))) && (_jumped.find(n_square_j) == _jumped.end()))
							{
								n_square = square - 2 * _dark_width - 1;
								if ((n_square == starting_square) || (get_value(_board,n_square) == 0))
									break;
							}
						}
						[[fallthrough]];
					case 1:
						dir = 2; // Update dir for next search
						if ((line > 1) && (col < (_width - 2)))
						{
							n_square_j = square - _dark_width + 1 - even_line;
							value = get_value(_board,n_square_j);
							if (((value == INV_COLOR) || (value == (INV_COLOR+2))) && (_jumped.find(n_square_j) == _jumped.end()))
							{
								n_square = square - 2 * _dark_width + 1;
								if ((n_square == starting_square) || (get_value(_board,n_square) == 0))
									break;
							}
						}
						[[fallthrough]];
					case 2:
						dir = 3; // Update dir for next search
						if ((line < (_height - 2)) && (col < (_width - 2)))
						{
							n_square_j = square + _dark_width + 1 - even_line;
							value = get_value(_board,n_square_j);
							if (((value == INV_COLOR) || (value == (INV_COLOR+2))) && (_jumped.find(n_square_j) == _jumped.end()))
							{
								n_square = square + 2 * _dark_width + 1;
								if ((n_square == starting_square) || (get_value(_board,n_square) == 0))
									break;
							}
						}
						[[fallthrough]];
					case 3:
						dir = 4; // Update dir for next search
						if ((line < (_height - 2)) && (col > 1))
						{
							n_square_j = square + _dark_width - even_line;
							value = get_value(_board,n_square_j);
							if (((value == INV_COLOR) || (value == (INV_COLOR+2))) && (_jumped.find(n_square_j) == _jumped.end()))
							{
								n_square = square + 2 * _dark_width - 1;
								if ((n_square == starting_square) || (get_value(_board,n_square) == 0))
									break;
							}
						}
						[[fallthrough]];
					case 4:
						// No possible move from this square.
						// If we moved more than the required length, then the move is valid and we return.
						// Otherwise, we close the square and go back to the previous one.
						if (move_done && (cur_move_length >= _move_length))
						{
							_move_length = cur_move_length;
							return;
						}
						_jumped.erase(_move.back().third);
						_move.pop_back();
						--cur_move_length;
						continue; // Backtrack to previous unclosed square
				}
				assert(cur_move_length <= _width * _height); // Just to be sure that cur_move_length is well formed because of first initial value
				assert(n_square != 255u);
				assert(n_square_j != 255u);
				_jumped.insert(n_square_j);
				_move.push_back({n_square, 0, n_square_j, _end});
				++cur_move_length;
				move_done = true;
			}
		}

		/**
		 * Search for next possible move according to checkers rules
		 */
		void search_next_move()
		{
			assert(!_move.empty());

			while (true)
			{
				auto initial_square = _move.front().first;
				auto initial_dir = _move.front().second;
				auto value = get_value(_board,initial_square);
				assert((value == COLOR) || (value == (COLOR + 2)));

				// If we starting search for this square or if we have a previous jumped piece
				if ((_move.size() == 1) || (_move.back().third != _end))
				{
					// We first search for move with a jump (i.e. catching at least an opponent piece)
					if (value == COLOR)
						search_next_jump_piece_move(initial_square);
					else
						search_next_jump_flying_move(initial_square);

				}

				// If we search for a move without jump
				if (_move_length == 0)
				{
					assert(_move.empty() || (_move.size() == 2));
					if (value == (COLOR + 2)) // Initial square is a flying piece
					{
						std::uint8_t last_landed_square = initial_square;
						if (_move.size() == 2) // We have a previous move from this square
							last_landed_square = _move.back().first;

						_move.clear();
						auto line = 2 * last_landed_square / _width;
						auto even_line = line % 2;
						auto col = (2 * last_landed_square) % _width + (1 - even_line);

						// Search for a flying landed move
						switch (initial_dir) // check in all possible directions
						{
							case 0:
								if ((line > 0) && (col > 0))
								{
									std::uint8_t n_square = last_landed_square - _dark_width - even_line;
									if (get_value(_board,n_square) == 0)
									{
										_move.push_back({initial_square, 0, _end, _end});
										_move.push_back({n_square, 4, _end, _end});
										break;
									}
								}
								last_landed_square = initial_square;
								line = 2 * last_landed_square / _width;
								even_line = line % 2;
								col = (2 * last_landed_square) % _width + (1 - even_line);
								[[fallthrough]];
							case 1:
								if ((line > 0) && (col < (_width - 1)))
								{
									std::uint8_t n_square = last_landed_square - _dark_width + 1 - even_line;
									if (get_value(_board,n_square) == 0)
									{
										_move.push_back({initial_square, 1, _end, _end});
										_move.push_back({n_square, 4, _end, _end});
										break;
									}
								}
								last_landed_square = initial_square;
								line = 2 * last_landed_square / _width;
								even_line = line % 2;
								col = (2 * last_landed_square) % _width + (1 - even_line);
								[[fallthrough]];
							case 2:
								if ((line < (_height - 1)) && (col < (_width - 1)))
								{
									std::uint8_t n_square = last_landed_square + _dark_width + 1 - even_line;
									if (get_value(_board,n_square) == 0)
									{
										_move.push_back({initial_square, 2, _end, _end});
										_move.push_back({n_square, 4, _end, _end});
										break;
									}
								}
								last_landed_square = initial_square;
								line = 2 * last_landed_square / _width;
								even_line = line % 2;
								col = (2 * last_landed_square) % _width + (1 - even_line);
								[[fallthrough]];
							case 3:
								if ((line < (_height - 1)) && (col > 0))
								{
									std::uint8_t n_square = last_landed_square + _dark_width - even_line;
									if (get_value(_board,n_square) == 0)
									{
										_move.push_back({initial_square, 3, _end, _end});
										_move.push_back({n_square, 4, _end, _end});
										break;
									}
								}
						}
					} else { // Initial square is a normal piece
						_move.clear();
						auto line = 2 * initial_square / _width;
						auto even_line = line % 2;
						auto col = (2 * initial_square) % _width + (1 - even_line);

						// Search for a single oriented-step move
						if (COLOR == LIGHT)
						{
							if (initial_dir < 2) initial_dir = 2; // Light pieces only do down
							switch (initial_dir)
							{
								case 2:
									if ((line < (_height - 1)) && (col < (_width - 1)))
									{
										std::uint8_t n_square = initial_square + _dark_width + 1 - even_line;
										value = get_value(_board,n_square);
										if (get_value(_board,n_square) == 0)
										{
											_move.push_back({initial_square, 3, _end, _end});
											_move.push_back({n_square, 4, _end, _end});
											break;
										}
									}
									[[fallthrough]];
								case 3:
									if ((line < (_height - 1)) && (col > 0))
									{
										std::uint8_t n_square = initial_square + _dark_width - even_line;
										value = get_value(_board,n_square);
										if (get_value(_board,n_square) == 0)
										{
											_move.push_back({initial_square, 4, _end, _end});
											_move.push_back({n_square, 4, _end, _end});
											break;
										}
									}
							}
						} else {
							assert(COLOR == DARK);
							switch (initial_dir)
							{
								case 0:
									if ((line > 0) && (col > 0))
									{
										std::uint8_t n_square = initial_square - _dark_width - even_line;
										value = get_value(_board,n_square);
										if (get_value(_board,n_square) == 0)
										{
											_move.push_back({initial_square, 1, _end, _end});
											_move.push_back({n_square, 4, _end, _end});
											break;
										}
									}
									[[fallthrough]];
								case 1:
									if ((line > 0) && (col < (_width - 1)))
									{
										std::uint8_t n_square = initial_square - _dark_width + 1 - even_line;
										value = get_value(_board,n_square);
										if (get_value(_board,n_square) == 0)
										{
											_move.push_back({initial_square, 4, _end, _end});
											_move.push_back({n_square, 4, _end, _end});
											break;
										}
									}
							}
						}
					}
				}

				if (!_move.empty())
				{
					// A move has been found we return
					// Ensure that move cannot be increased
					assert(_move.back().second == 4);
					assert(_move.size() >= 2);
					return;
				}

				// Search for the next square of the good color
				++initial_square;
				if (initial_square == _end) { assert(_move.empty()); return; } // End of board
				auto p = get_value(_board,initial_square);
				while ((p != COLOR) && (p != (COLOR + 2)))
				{
					++initial_square;
					if (initial_square == _end) { assert(_move.empty()); return; } // End of board
					p = get_value(_board,initial_square);
				}
				_move.push_back({initial_square, 0, _end, _end});
			}
		}
	};

	/**
	 * @brief Return an iterator on the first move of the board
	 * @return An iterator on the first move of the board
	 */
	iterator begin()
	{
		return iterator(_board,_width,_height);
	}

	/**
	 * @brief Return an iterator on the end of the board, i.e. with an empty move
	 * @return An iterator to the end of the board
	 */
	iterator end()
	{
		return iterator(_board);
	}

};

