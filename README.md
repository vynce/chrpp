**CHR++ :** *Integration of Constraint Handling Rules in C++*
===================

[![build status](https://gitlab.com/vynce/chrpp/badges/master/build.svg)](https://gitlab.com/vynce/chrpp/commits/master)

### Author

Vincent Barichard<br />
Computer Science - LERIA (University of Angers)<br />
http://blog.univ-angers.fr/leria/
 
### Comments, bugs, information

See our [Contributing doc](CONTRIBUTING.md) for information on how to report
issues, send a patch or suggest a new feature.

Get the documentation
=====================

You can browse online documentation [here](https://vynce.gitlab.io/chrpp) or download the corresponding [pdf file](https://vynce.gitlab.io/chrpp/chrpp_manual.pdf).

Quick installation under Unix (and GNU/Linux)
=============================================

## Requirements

To build both the `chrppc` compiler and your own *CHR++*  source file, you need to install a `C++` compiler able to compile modern C++ source files. You also need to install CMake (https://cmake.org/), a cross-platform tool designed to build and package software.

## Downloading and extracting files

To build *CHR++* , you first need to build the `chrppc` compiler.
First, Clone chrpp (or your fork) and enter it:

~~~~
$ git clone https://gitlab.com/vynce/chrpp.git
~~~~

This will create a directory `chrppc` containing the compiler sources, but also the header files needed to build your own CHR programs, documentation and examples programs.

## Installing the chrppc compiler and the examples

You can go to the extracted directory and can call the `cmake` program to create the build tree.

~~~~
$ cd chrpp
$ cmake .
~~~~

You can now build the `chrppc` compiler and all examples:

~~~~
$ make
~~~~

Then, you are ready to install the `chrppc` compiler, examples and all `C++` headers needed to build your own projects:

~~~~
$ make DESTDIR=/path/to/dir install
~~~~

If you not provide the `DESTDIR` parameter, the disk root '/' will be used instead.

You will find the `chrppc` compiler and binaries in the `/path/to/dir/usr/local/bin/` directory and the required `C++` headers in the `/path/to/dir/usr/local/include/chrpp` directory. You can safely removed the sources from your computer.

Create a quick project
======================

The previous sections tell how to compile and use the different parts of the *CHR++*  archive. Provided you already compile it and now want to create and compile your own programs, we now describe the steps to achieve it.

Before going further, your need to have the two following things:

- The `chrppc` compiler.  
If you followed the instructions before, the `chrppc` program must be located in the `/path/to/dir/usr/local/bin` directory.

- The path of the runtime header files.  
The header files must be located in the `/path/to/dir/usr/local/include/chrpp` directory.

Remind that a *CHR++*  source file does not contain only `C++` instructions but also CHR statements which can not be compiled straightly with any `C++` compiler. A mandatory preprocessing step is needed. That's the aim of the `chrppc` program. It is a compiler that preprocess any *CHR++*  file to generate pure `C++` file where all CHR statements have been converted to `C++` source code. The final step is to compile the generated `C++` source file with a `C++` compiler.

The first step is to create a *CHR++*  source file. It can be seen as a `C++` source file with CHR statements embedded. The following example is a very simple *CHR++*  program. It just prints `Hello!` when invoked. There is only one CHR constraint and one CHR rule.

~~~~
#include <iostream>
#include <chrpp.hh>

void hello()
{
	std::cout << "Hello!" << std::endl;
}

<CHR name="TEST">
	<chr_constraint> test()
	test() <=> hello();;
</CHR>

int main()
{
    auto space = TEST::create();

    space->test();
    return 0;
}
~~~~

To create the program you first need to preprocess the file to generate the transformed `C++` file. Then you have to compile it with a `C++` compiler in order to create the binary. As we want to add the trace instructions and print statistics, we must instruct the compiler with the right options:

~~~~
$ /path/to/dir/usr/local/bin/chrppc test.chrpp -sout > test.cpp
$ g++ -std=c++17 -I/path/to/dir/usr/local/include/chrpp test.cpp -o test
~~~~

Finally, you can execute the program by calling it at command line:

~~~~
$ ./test
Hello!
~~~~

The program runs well and print the `Hello!` sentence to the standard output.
For a bigger *CHR++*  project, you may need to use a build system as `cmake`. You can browse the `CMakeList.txt` files in the archive and use them to create your owns.
