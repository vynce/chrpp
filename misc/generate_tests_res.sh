#!/bin/bash

echo
echo "Generate results files for failing tests"
echo

STATE=0
RES_FILE_NAME=""
TMP_FILE="/tmp/chrpp_res_$$.txt"

while read -r line;
#while read -r -u9 line;
do
	echo $line
	case $STATE in
	0)
		# Not in test
		FOUND=NO
		[[ "$line" =~ -{10} ]] && STATE=1 && FOUND=YES
		if [ "$FOUND" == "YES" ]; then
			rm -f $TMP_FILE
			echo "$line" >> $TMP_FILE
		fi
		;;
	1 | 2 | 3 | 4)
		# In a test
		[[ "$line" =~ -{10} ]] && STATE=$(( $STATE + 1 ))
#		printf "%s" $line >> $TMP_FILE
		echo "$line" >> $TMP_FILE
		;;
	5)
		# Search for failure
		[[ "$line" =~ \#failure\ ([a-zA-Z0-9_]+) ]] && STATE=6 && RES_FILE_NAME="${BASH_REMATCH[1]}.res" 
		[[ "$line" =~ \#success ]] && STATE=0 

		if [ "$STATE" == "6" ]; then
			# Replace test result be new one
			if [ ! -e "${RES_FILE_NAME}" ]; then
				mv $TMP_FILE $RES_FILE_NAME
			fi
			STATE=0
		fi
		;;
	esac
done 9< <($@) 
