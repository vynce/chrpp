Contributing
============

We are very happy and grateful if you are willing to help us improve CHR++. You can do it in many ways.

## Reporting issues

If you find an issue, you can use our [Issue
Tracker](https://gitlab.com/vynce/chrpp/issues). Make sure that it
hasn't yet been reported by searching first.

Remember to include the following information:

* CHR++ version
* Steps to reproduce the issue

## Submit a patch

If you would like to share your improvements, you can provide your patch by using one the following ways:

1. If you use a public accessible gitlab repository, please send us a merge request. This is the preferred way
2. If you don't have access to a gitlab repository, you can send a patch by email to [Vincent Barichard](http://www.info.univ-angers.fr/pub/barichar/page_perso/index.php?lang=en&page=accueil).

## Contributor license agreement

By submitting code you agree to the CHR++ license agreement.
Thank you for supporting CHR++!
