digraph Compilation_multi {
	rankdir=LR
	margin=0
	nodesep=0.4

	A [label=<void hello()<BR ALIGN="LEFT"/>
\{<BR ALIGN="LEFT"/>
  std::cout &lt;&lt; "Hello!" &lt;&lt; std::endl;<BR ALIGN="LEFT"/>
\}<BR ALIGN="LEFT"/>
<BR ALIGN="LEFT"/>
&lt;CHR name="TEST1"&gt;<BR ALIGN="LEFT"/>
  &lt;chr_constraint&gt; test()<BR ALIGN="LEFT"/>
  test() &lt;=&gt; hello();;<BR ALIGN="LEFT"/>
&lt;/CHR&gt;<BR ALIGN="LEFT"/>
<BR ALIGN="LEFT"/>
void hi()<BR ALIGN="LEFT"/>
\{<BR ALIGN="LEFT"/>
  std::cout &lt;&lt; "Hi!" &lt;&lt; std::endl;<BR ALIGN="LEFT"/>
\}<BR ALIGN="LEFT"/>
<BR ALIGN="LEFT"/>
&lt;CHR name="TEST2"&gt;<BR ALIGN="LEFT"/>
  &lt;chr_constraint&gt; test()<BR ALIGN="LEFT"/>
  test() &lt;=&gt; hi();;<BR ALIGN="LEFT"/>
&lt;/CHR&gt;<BR ALIGN="LEFT"/>
<BR ALIGN="LEFT"/>
int main()<BR ALIGN="LEFT"/>
\{<BR ALIGN="LEFT"/>
  ....<BR ALIGN="LEFT"/>
  return 0;<BR ALIGN="LEFT"/>
\}<BR ALIGN="LEFT"/>
		| <font face="Deja vu sans">CHR++ source file</font>>
		,shape="Mrecord", fontname="Deja vu sans mono", fontsize=8, style="rounded,filled", fillcolor=lavender]

	B [label=<void hello()<BR ALIGN="LEFT"/>
\{<BR ALIGN="LEFT"/>
  std::cout &lt;&lt; "Hello!" &lt;&lt; std::endl;<BR ALIGN="LEFT"/>
\}<BR ALIGN="LEFT"/>
class TEST1<BR ALIGN="LEFT"/>
\{<BR ALIGN="LEFT"/>
  .....<BR ALIGN="LEFT"/>
\};<BR ALIGN="LEFT"/>
		| <font face="Deja vu sans">TEST1.hh</font>>
		,shape="Mrecord", fontname="Deja vu sans mono", fontsize=8, style="rounded,filled", fillcolor=lavender]

	BB [label=<#include &lt;TEST1.hh&gt;<BR ALIGN="LEFT"/>
chr::ES_CHR TEST1::do_test(....)<BR ALIGN="LEFT"/>
\{<BR ALIGN="LEFT"/>
  ....<BR ALIGN="LEFT"/>
\}<BR ALIGN="LEFT"/>
		| <font face="Deja vu sans">TEST1.cpp</font>>
		,shape="Mrecord", fontname="Deja vu sans mono", fontsize=8, style="rounded,filled", fillcolor=lavender]

	C [label=<#include &lt;TEST1.hh&gt;<BR ALIGN="LEFT"/>
void hi()<BR ALIGN="LEFT"/>
\{<BR ALIGN="LEFT"/>
  std::cout &lt;&lt; "Hi!" &lt;&lt; std::endl;<BR ALIGN="LEFT"/>
\}<BR ALIGN="LEFT"/>
class TEST2<BR ALIGN="LEFT"/>
\{<BR ALIGN="LEFT"/>
  .....<BR ALIGN="LEFT"/>
\};<BR ALIGN="LEFT"/>
		| <font face="Deja vu sans">TEST2.hh</font>>
		,shape="Mrecord", fontname="Deja vu sans mono", fontsize=8, style="rounded,filled", fillcolor=lavender]

	CC [label=<#include &lt;TEST1.hh&gt;<BR ALIGN="LEFT"/>
chr::ES_CHR TEST2::do_test(....)<BR ALIGN="LEFT"/>
\{<BR ALIGN="LEFT"/>
  ....<BR ALIGN="LEFT"/>
\}<BR ALIGN="LEFT"/>
		| <font face="Deja vu sans">TEST2.cpp</font>>
		,shape="Mrecord", fontname="Deja vu sans mono", fontsize=8, style="rounded,filled", fillcolor=lavender]


	D [label=<#include &lt;TEST1.hh&gt;<BR ALIGN="LEFT"/>
#include &lt;TEST2.hh&gt;<BR ALIGN="LEFT"/>
int main()<BR ALIGN="LEFT"/>
\{<BR ALIGN="LEFT"/>
  ....<BR ALIGN="LEFT"/>
  return 0;<BR ALIGN="LEFT"/>
\}<BR ALIGN="LEFT"/>
		| <font face="Deja vu sans">test_chr.cpp</font>>
		,shape="Mrecord", fontname="Deja vu sans mono", fontsize=8, style="rounded,filled", fillcolor=lavender]

	E [label="Program\nbinary\n"
		,shape="box", fontname="Deja vu sans", fontsize=12, style="rounded,filled", fillcolor=navajowhite, margin=0.3]

	A -> B [label="Preprocess with\nchrppc", fontname="Deja vu sans", fontsize=10]
	A -> C
	A -> D
	B -> BB [label="C++ generated output files",fontname="Deja vu sans",fontsize=10,style=dashed,arrowhead=none,penwidth=5,color=lightgrey,constraint=false]
	B -> E
	BB -> E [label="Compile with a\nC++ compiler", fontname="Deja vu sans", fontsize=10]
	C -> CC [style=dashed,arrowhead=none,penwidth=5,color=lightgrey,constraint=false]
	C -> E
	CC -> E
	D -> E
}
