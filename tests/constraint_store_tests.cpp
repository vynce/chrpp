/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <array>
#include <set>
#include <fstream>
#include <sstream>
#include <string>
#include <cerrno>

#include <runtime_tests.hh>
#include <chrpp.hh>

namespace tests {
	namespace constraint_store {

		template< typename T >
		size_t cs_size(T& cs)
		{
			size_t count = 0;
			auto it = cs.begin();
			while (!it.at_end())
			{
				++count;
				++it;
			}
			return count;
		}

		template< typename T >
		std::string str_statistics(T& cs, typename T::Statistics& stats)
		{
			std::string res;
			stats = cs.index_statistics();
			res += "Constraints(" + std::to_string(stats.size) + "):";
			auto it = cs.begin();
			while (!it.at_end())
			{
				res += " " + it.to_string();
				++it;
			}
			res += "\n";
		
			res += "Indexes (pending=" + std::to_string(stats.nb_pending) + "):\n";
			unsigned int n_idx = 0;
			for (auto idx_m : stats.indexes)
			{
				res += "  Idx " + std::to_string(n_idx) + "\n";
				for (auto e : idx_m)
				{
					res += "    [" + chr::TIW::to_string(e.first) + "]:";
					for (unsigned int j = 0; j < e.second.size(); ++j)
						res += + " " + chr::TIW::to_string(e.second[j]);
					res += "\n";
				}
				++n_idx;
			}
			return res;
		}

		/**
		 * *************************************************************
		 * Data structures needed to make some tests on Constraint_store
		 * *************************************************************
		 */
		using DummyConstraint = std::tuple<unsigned long int>;
		using SimpleConstraint = std::tuple< unsigned long int, chr::Logical_var_ground<int> >;
		using SimpleConstraint2 = std::tuple< unsigned long int, chr::Logical_var_ground<int>, chr::Logical_var<int> >;
		using IndexedConstraintStore = chr::Constraint_store_index< SimpleConstraint2, std::tuple< chr::LNS::Index<0,1>, chr::LNS::Index<0>, chr::LNS::Index<1> > >;
		using IndexedConstraintStorePersistent = chr::Constraint_store_index< SimpleConstraint2, std::tuple< chr::LNS::Index<0,1>, chr::LNS::Index<0>, chr::LNS::Index<1> >, false >;

		/**
		 * Test some pushes in Constraint_store
		 */
		void simple_insert_tests()
		{
			auto v1 = chr::Logical_var_ground<int>(101);
			auto v2 = chr::Logical_var_ground<int>(105);
			auto v3 = chr::Logical_var_ground<int>(103);
			SimpleConstraint cc1(1,v1), cc2(2,v2), cc3(3,v3);
			auto cs = chr::make_shared< chr::Constraint_store_simple< SimpleConstraint > >();

			ASSERT( cs->empty() );

			cs->add( cc1 );
			cs->add( cc2 );

			ASSERT( !cs->empty() && (cs_size(*cs) == 2) && (cs_size(*cs) == cs->size()));

			cs->add( cc3 );
			ASSERT( !cs->empty() && (cs_size(*cs) == 3) && (cs_size(*cs) == cs->size()));
		}

		/**
		 * Check that iterators on Constraint_store visit all elements
		 */
		void simple_iterators()
		{
			auto cs = chr::make_shared< chr::Constraint_store_simple<SimpleConstraint> >();
			std::set< unsigned long int > ids;

			int j = 4;
			for (unsigned int i=1; i < 11; ++i)
			{
				auto vi = chr::Logical_var_ground<int>(104+j); // Set a value, don't care about the value
				auto c_vi = SimpleConstraint(i,vi);
				ASSERT(std::get<0>(*cs->add( c_vi)) == i);
				ids.insert(i);
				j += 5;
			}

			auto it = cs->begin();
			std::set< unsigned long int > ids_copy = ids;
			for ( ; !it.at_end(); ++it)
			{
				ids_copy.erase(std::get<0>(*it));
			}
			ASSERT( ids_copy.empty() );
		}

		/**
		 * Test some pushes in Constraint_store
		 */
		void index_insert_tests()
		{
			auto cs = chr::make_shared< IndexedConstraintStore >();
			IndexedConstraintStore::Statistics stats;
			chr::Logical_var_ground< int > X(118);
			chr::Logical_var< int > Y, Z, W;
			SimpleConstraint2 cc1(1,X,Y), cc2(2,X,Z), cc3(3,X,W);
			TRACE( std::cout << "Variables: X=" << X.to_string() << " Y=" << Y.to_string() << " Z=" << Z.to_string() << " W=" << W.to_string() << std::endl; )
			ASSERT(cs->empty() && (cs_size(*cs) == 0) && (cs_size(*cs) == cs->size()));

			cs->add( cc1 );
			TRACE( std::cout << str_statistics(*cs,stats) << std::endl; )
			cs->add( cc2 );
			TRACE( std::cout << str_statistics(*cs,stats) << std::endl; )
			ASSERT(!cs->empty() && (cs_size(*cs) == 2) && (cs_size(*cs) == cs->size()));
			cs->add( cc3 );
			ASSERT(!cs->empty() && (cs_size(*cs) == 3) && (cs_size(*cs) == cs->size()));
			TRACE( std::cout << str_statistics(*cs,stats) << std::endl; )

			str_statistics(*cs,stats);
			ASSERT((stats.size == cs_size(*cs)) && (cs_size(*cs) == cs->size()));
			ASSERT(stats.indexes[0].size() == 3);
			ASSERT(stats.indexes[1].size() == 1);
			ASSERT(stats.indexes[2].size() == 3);

			std::vector< size_t > stats_indexed_variables_size;
			for (auto idx_m : stats.indexes)
				for (auto e : idx_m)
					stats_indexed_variables_size.push_back(e.second.size());

			std::sort(std::begin(stats_indexed_variables_size), std::end(stats_indexed_variables_size));
			ASSERT(stats_indexed_variables_size.size() == 7);
			ASSERT(stats_indexed_variables_size[0] == 1);
			ASSERT(stats_indexed_variables_size[1] == 1);
			ASSERT(stats_indexed_variables_size[2] == 1);
			ASSERT(stats_indexed_variables_size[3] == 1);
			ASSERT(stats_indexed_variables_size[4] == 1);
			ASSERT(stats_indexed_variables_size[5] == 1);
			ASSERT(stats_indexed_variables_size[6] == 3);

			TRACE( std::cout << "Kill cc3" << std::endl; )
			auto it = cs->begin();
			it.kill();
			TRACE( std::cout << str_statistics(*cs,stats) << std::endl; )

			ASSERT((cs_size(*cs) == 2) && (cs_size(*cs) == cs->size()));
		}

		/**
		 * Check unification (indexes updates) on constraint stores with indexes.
		 */
		void index_unification()
		{
			auto cs = chr::make_shared< IndexedConstraintStore >();
			IndexedConstraintStore::Statistics stats;
			chr::Logical_var_ground< int > X(118),U(119);
			chr::Logical_var< int > Y,Z,W;
			SimpleConstraint2 cc1(1,X,Y), cc2(2,X,Z), cc3(3,X,W), cc4(4,U,Y);
			TRACE( std::cout << "Variables: U=" << U.to_string() << " W=" << W.to_string() << " X=" << X.to_string() << " Y=" << Y.to_string() << " Z=" << Z.to_string() << std::endl; )

			cs->add( cc1 );
			cs->add( cc2 );
			cs->add( cc3 );
			cs->add( cc4 );
			ASSERT(!cs->empty() && (cs_size(*cs) == 4) && (cs_size(*cs) == cs->size()));
			TRACE( std::cout << str_statistics(*cs,stats) << std::endl; )

			TRACE( std::cout << "Update Z %= " << Y << std::endl; )
			Z %= Y;
			TRACE( std::cout << str_statistics(*cs,stats) << std::endl; )
			auto it1_a = cs->begin<1>(Z);
			TRACE( std::cout << "Z in"; )
			while (!it1_a.at_end())
			{
				TRACE( std::cout << " " << it1_a.to_string(); )
				++it1_a;
			}
			auto it1_b = cs->begin<2>(Z);
			while (!it1_b.at_end())
			{
				TRACE( std::cout << " " << it1_b.to_string(); )
				++it1_b;
			}
			std::cout << std::endl;

			TRACE( std::cout << "Update Z %= 3" << std::endl; )
			Z %= 3;
			TRACE( std::cout << str_statistics(*cs,stats) << std::endl; )

			TRACE(
				auto it2_a = cs->begin<1>(Z);
				std::cout << "Z in";
				while (!it2_a.at_end())
				{
					std::cout << " " << it2_a.to_string();
					++it2_a;
				}
				auto it2_b = cs->begin<2>(Z);
				while (!it2_b.at_end())
				{
					std::cout << " " << it2_b.to_string();
					++it2_b;
				}
				std::cout << std::endl;
			)
			ASSERT( Y == 3 );

			TRACE( std::cout << "Kill constraints" << std::endl; )
			auto it_constraint = cs->begin();
			while(!it_constraint.at_end())
			{
				auto it_bak = it_constraint;
				++it_constraint;
				it_bak.kill();
			}
			TRACE( std::cout << str_statistics(*cs,stats) << std::endl; )

			str_statistics(*cs,stats);
			ASSERT(stats.constraints.size() == 0);
			std::vector< size_t > stats_indexed_variables_size;
			for (auto idx_m : stats.indexes)
				ASSERT(idx_m.size() == 0);
		}

		/**
		 * Miscallenous tests
		 */
		void index_misc_tests()
		{
			auto cs = chr::make_shared< IndexedConstraintStore >();
			IndexedConstraintStore::Statistics stats;
			chr::Logical_var_ground< int > X(118);
			chr::Logical_var< int > Y,Z;
			SimpleConstraint2 cc1(1,X,Y), cc2(2,X,Y), cc3(3,X,Z);
			TRACE( std::cout << "Variables: X=" << X.to_string() << " Y=" << Y.to_string() << " Z=" << Z.to_string() << std::endl; )

			// Check that adding two constraints with same key don't crash when vars are unified
			cs->add( cc1 );
			cs->add( cc2 );
			cs->add( cc3 );
			ASSERT(!cs->empty() && (cs_size(*cs) == 3) && (cs_size(*cs) == cs->size()));
			TRACE( std::cout << str_statistics(*cs,stats) << std::endl; )

			// Unification
			Y %= Z;
			auto str_stats = str_statistics(*cs,stats);
			TRACE( std::cout << str_stats << std::endl; )
			std::vector< size_t > stats_indexed_variables_size;
			for (auto idx_m : stats.indexes)
				for (auto e : idx_m)
					stats_indexed_variables_size.push_back(e.second.size());

			std::sort(std::begin(stats_indexed_variables_size), std::end(stats_indexed_variables_size));
			ASSERT(stats_indexed_variables_size.size() == 3);
			ASSERT(stats_indexed_variables_size[0] == 3);
			ASSERT(stats_indexed_variables_size[1] == 3);
			ASSERT(stats_indexed_variables_size[2] == 3);
		}

		/**
		 * Backtrack tests on Constraint_store_simple
		 */
		void simple_backtrack_tests()
		{
			using namespace chr;
			std::string str1, str2, str3;
			auto cs = chr::make_shared< chr::Constraint_store_simple<DummyConstraint> >();

			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			Depth_t d = Backtrack::depth();

			Backtrack::inc_backtrack_depth();
			for (unsigned int i=1; i < 11; ++i)
				ASSERT(std::get<0>(*cs->add(DummyConstraint(i))) == i);

			TRACE(
				std::cout << "New choice add" << std::endl;
				std::cout << "Content :";
			)
			for (auto it = cs->begin() ; !it.at_end(); ++it)
			{
				TRACE( std::cout << " " << it.to_string(); )
				str1 += it.to_string();
			}
			TRACE( std::cout << std::endl; )

			Backtrack::inc_backtrack_depth();
			auto it = cs->begin();
			it.kill();

			TRACE(
				std::cout << "New choice kill" << std::endl;
				std::cout << "Content :";
			)
			for (auto it = cs->begin() ; !it.at_end(); ++it)
			{
				TRACE( std::cout << " " << it.to_string(); )
				str2 += it.to_string();
			}
			TRACE( std::cout << std::endl; )

			Backtrack::inc_backtrack_depth();
			auto it2 = cs->begin();
			++it2;
			it2.kill();

			TRACE(
				std::cout << "New choice kill again" << std::endl;
				std::cout << "Content :";
				for (auto it = cs->begin() ; !it.at_end(); ++it)
					std::cout << " " << it.to_string();
				std::cout << std::endl;
			)

			TRACE( std::cout << "Then add" << std::endl; )
			ASSERT(std::get<0>(*cs->add( DummyConstraint(20))) == 20);
			TRACE(
				std::cout << "Content :";
				for (auto it = cs->begin() ; !it.at_end(); ++it)
					std::cout << " " << it.to_string();
				std::cout << std::endl;
			)

			Backtrack::back_to(d+1);
			TRACE(
				std::cout << "Back to previous / previous" << std::endl;
				std::cout << "Content :";
			)
			for (auto it = cs->begin() ; !it.at_end(); ++it)
			{
				TRACE( std::cout << " " << it.to_string(); )
				str3 += it.to_string();
			}
			TRACE( std::cout << std::endl; )
					std::cout << "str3=" << str3 << std::endl;
			std::cout << "str1=" << str1 << std::endl;
			ASSERT(str3 == str1);

			Backtrack::back_to(d);
			TRACE(
				std::cout << "Back to first" << std::endl;
				std::cout << "Content :";
			)
			str3 = std::string();
			for (auto it = cs->begin() ; !it.at_end(); ++it)
			{
				TRACE( std::cout << " " << it.to_string(); )
				str3 += it.to_string();
			}
			TRACE( std::cout << std::endl; )
			ASSERT(str3.empty());
		}

		/**
		 * Backtrack tests on Constraint_store_index
		 */
		void index_backtrack_tests()
		{
			using namespace chr;
			auto cs = chr::make_shared< IndexedConstraintStore >();
			IndexedConstraintStore::Statistics stats;

			chr::Logical_var_ground< int > U(117),X(118);
			chr::Logical_var< int > Y, Z, W;
			SimpleConstraint2 cc1(1,X,Y), cc2(2,X,Z), cc3(3,X,W), cc4(4,U,W);
			std::string str1, str2, str3, str_res;

			TRACE( std::cout << "Variables: U=" << U.to_string() << " X=" << X.to_string() << " Y=" << Y.to_string() << " Z=" << Z.to_string() << " W=" << W.to_string() << std::endl; )

			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			Depth_t d = Backtrack::depth();
			assert(d == 0);

			str1 = str_statistics(*cs,stats);

			Backtrack::inc_backtrack_depth();
			cs->add(cc1);
			cs->add(cc2);
			cs->add(cc3);
			cs->add(cc4);
			TRACE( std::cout << "New choice, add constraints" << std::endl; )
			ASSERT(!cs->empty() && (cs_size(*cs) == 4) && (cs_size(*cs) == cs->size()));
			str2 = str_statistics(*cs,stats);
			TRACE( std::cout << str2 << std::endl; )

			Backtrack::inc_backtrack_depth();
			Z %= 3;
			TRACE( std::cout << "New choice, update Z %= " << Z << std::endl; )
			str3 = str_statistics(*cs,stats);
			TRACE( std::cout << str3 << std::endl; )

			{
				TRACE(
					auto it1 = cs->begin<2>(Z);
					std::cout << "Z in";
					while (!it1.at_end())
					{
						std::cout << " " << it1.to_string();
						++it1;
					}
					auto it2 = cs->begin<1>(Z);
					while (!it2.at_end())
					{
						std::cout << " " << it2.to_string();
						++it2;
					}
					std::cout << std::endl;
				)
			}

			Backtrack::inc_backtrack_depth();
			TRACE( std::cout << "New choice, kill constraints" << std::endl; )
			auto it_constraint = cs->begin();
			while (!it_constraint.at_end())
			{
				std::cout << it_constraint.to_string() << std::endl;
				it_constraint.lock();
				it_constraint.kill();
				it_constraint.next_and_unlock();
			}
			TRACE( std::cout << str_statistics(*cs,stats) << std::endl; )

			{
				TRACE( std::cout << "Browse store" << std::endl; )
				auto it = cs->begin<1>(X);
				TRACE( std::cout << "X in"; )
				while (!it.at_end())
				{
					TRACE( std::cout << " " << it.to_string(); )
					++it;
				}
				TRACE( std::cout << std::endl; )
			}
			{
				auto it = cs->begin<1>(U);
				TRACE( std::cout << "U in"; )
				while (!it.at_end())
				{
					TRACE( std::cout << " " << it.to_string(); )
					++it;
				}
				TRACE( std::cout << std::endl; )
			}
			{
				auto it = cs->begin<2>(Z);
				TRACE( std::cout << "Z in"; )
				while (!it.at_end())
				{
					TRACE( std::cout << " " << it.to_string(); )
					++it;
				}
				TRACE( std::cout << std::endl; )
			}

			str_statistics(*cs,stats);
			ASSERT(stats.indexes[0].size() == 5);
			for (auto x : stats.indexes[0])
				ASSERT(x.second.empty());
			ASSERT(stats.indexes[1].size() == 2);
			for (auto x : stats.indexes[1])
				ASSERT(x.second.empty());
			ASSERT(stats.indexes[2].size() == 4);
			for (auto x : stats.indexes[2])
				ASSERT(x.second.empty());

			Backtrack::back_to(d+2);
			TRACE( std::cout << "Back to previous" << std::endl; )
			str_res = str_statistics(*cs,stats);
			TRACE( std::cout << str_res << std::endl; )
			ASSERT(str3 == str_res);

			Backtrack::back_to(d+1);
			TRACE( std::cout << "Back to previous" << std::endl; )
			str_res = str_statistics(*cs,stats);
			TRACE( std::cout << str_res << std::endl; )
			ASSERT(str2 == str_res);

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to first" << std::endl; )
			str_res = str_statistics(*cs,stats);
			TRACE( std::cout << str_res << std::endl; )
			ASSERT(str1 == str_res);
		}

		/**
		 * Backtrack persistent tests on Constraint_store_index
		 */
		void index_backtrack_persistent_tests()
		{
			using namespace chr;
			auto cs = chr::make_shared< IndexedConstraintStorePersistent >();
			IndexedConstraintStorePersistent::Statistics stats;

			chr::Logical_var_ground< int > U(117),X(118);
			chr::Logical_var< int > Y, Z, W;
			SimpleConstraint2 cc1(1,X,Y), cc2(2,X,Z), cc3(3,X,W), cc4(4,U,W);
			std::string str1, str2, str3, str_res;

			TRACE( std::cout << "Variables: U=" << U.to_string() << " X=" << X.to_string() << " Y=" << Y.to_string() << " Z=" << Z.to_string() << " W=" << W.to_string() << std::endl; )

			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			Depth_t d = Backtrack::depth();
			assert(d == 0);

			str1 = str_statistics(*cs,stats);

			Backtrack::inc_backtrack_depth();
			cs->add(cc1);
			cs->add(cc2);
			TRACE( std::cout << "New choice, add constraints" << std::endl; )
			ASSERT(!cs->empty() && (cs_size(*cs) == 2) && (cs_size(*cs) == cs->size()));
			str2 = str_statistics(*cs,stats);
			TRACE( std::cout << str2 << std::endl; )

			Backtrack::inc_backtrack_depth();
			cs->add(cc3);
			cs->add(cc4);
			TRACE( std::cout << "New choice, add constraints" << std::endl; )
			ASSERT(!cs->empty() && (cs_size(*cs) == 4) && (cs_size(*cs) == cs->size()));
			str3 = str_statistics(*cs,stats);
			TRACE( std::cout << str3 << std::endl; )

			Backtrack::back_to(d+1);
			TRACE( std::cout << "Back to previous" << std::endl; )
			str_res = str_statistics(*cs,stats);
			TRACE( std::cout << str_res << std::endl; )
			ASSERT(str3 == str_res);

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to first" << std::endl; )
			str_res = str_statistics(*cs,stats);
			TRACE( std::cout << str_res << std::endl; )
			ASSERT(str3 == str_res);
		}

	}

	void run_constraint_store_tests(cute::suite& s)
	{
		s.push_back( { "Constraint store simple, insert and kill tests", constraint_store::simple_insert_tests } );
		s.push_back( { "Constraint store simple iterator", constraint_store::simple_iterators } );
		s.push_back( { "Constraint store simple, backtrack tests", constraint_store::simple_backtrack_tests } );
		s.push_back( { "Constraint store with index, add and kill tests", constraint_store::index_insert_tests } );
		s.push_back( { "Constraint store with index, unification tests", constraint_store::index_unification } );
		s.push_back( { "Constraint store with index, misc tests", constraint_store::index_misc_tests } );
		s.push_back( { "Constraint store with index, backtrack tests", constraint_store::index_backtrack_tests } );
		s.push_back( { "Constraint store with index (persistent), backtrack tests", constraint_store::index_backtrack_persistent_tests } );
	}
}
