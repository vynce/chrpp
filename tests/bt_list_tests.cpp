/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <array>
#include <set>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <cerrno>

#include <runtime/trace.hh>
#include <runtime_tests.hh>
#include <runtime/bt_list.hh>

namespace tests {
	namespace bt_list {
		chr::Bt_list<int> test_move_constructor(chr::Bt_list<int> l)
		{
			return l;
		}

		/**
		 * Test some inserts and removes in Bt_list
		 */
		void insert_remove_tests()
		{
			using namespace chr;
			Bt_list<int> ul1;
			std::vector< Bt_list<int>::iterator > v_iterators;

			ASSERT(ul1.empty());
			for (int i=0; i < 10; ++i)
			{
				auto it = ul1.insert(i);
				ASSERT(*it == i);
				if ((i%2) == 0)
					v_iterators.push_back(it);
			}

			TRACE( std::cout << ul1.to_string() << std::endl; )
			ASSERT(ul1.size() == 10);

			for (auto i : v_iterators)
			{
				i.remove();
			}
			TRACE( std::cout << ul1.to_string() << std::endl; )
			ASSERT(ul1.size() == 5);

			for (int i=20; i < 23; ++i)
			{
				ul1.insert(i);
			}
			TRACE( std::cout << ul1.to_string() << std::endl; )
			ASSERT(ul1.size() == 8);

			Bt_list<int> ul2;
			std::vector< Bt_list<int>::iterator > v_iterators2;
			ASSERT(ul2.empty());
			for (int i=0; i < 10; ++i)
			{
				auto it = ul2.insert(i);
				if ((i%2) == 0)
					v_iterators2.push_back(it);
			}
			TRACE( std::cout << ul2.to_string() << std::endl; )
			ASSERT(ul2.size() == 10);

			ASSERT(ul2._first_free == Bt_list<int>::END_LIST);
			for (auto i : v_iterators2)
			{
				i.remove();
			}

			// Count nb empty
			auto nb_empty = 0;
			auto p = ul2._first_free;
			while (p != Bt_list<int>::END_LIST)
			{
				++nb_empty;
				p = ul2.data(p)._prev;
			}
			std::cout << ul2.to_string(true) << std::endl;
			ASSERT(nb_empty == 5);

			TRACE( std::cout << ul2.to_string() << std::endl; )
			ASSERT(ul2.size() == 5);
			for (int i=20; i < 23; ++i)
				ul2.insert(i);
			TRACE( std::cout << ul2.to_string() << std::endl; )
			ASSERT(ul2.size() == 8);

			// Count nb empty
			nb_empty = 0;
			p = ul2._first_free;
			while (p != Bt_list<int>::END_LIST)
			{
				++nb_empty;
				p = ul2.data(p)._prev;
			}
			TRACE( std::cout << ul2.to_string(true) << std::endl; )
			ASSERT(nb_empty == 2);

			Bt_list<int> ul3 = std::move(ul1);
			ASSERT(ul3.size() == 8);

			Bt_list<int> ul4 = test_move_constructor(Bt_list<int>());
			ASSERT(ul4.size() == 0);

			Bt_list<int> ul5 = ul3.clone();
			ASSERT(ul3.to_string(1) == ul5.to_string(1));
		}

		/**
		 * Test some replacements in Bt_list
		 */
		void replace_tests()
		{
			using namespace chr;
			Bt_list<int> ul1;
			std::vector< Bt_list<int>::iterator > v_pids;

			ASSERT(ul1.empty());
			for (int i=0; i < 10; ++i)
			{
				auto it = ul1.insert(i);
				ASSERT((*it) == i);
				if ((i%2) == 0)
				{
					v_pids.push_back(it);
				}
			}

			TRACE( std::cout << ul1.to_string() << std::endl; )
			ASSERT(ul1.size() == 10);

			int k=100;
			for (auto i : v_pids)
			{
				ul1.insert_before(i.pid(), k++);
				ul1.replace(i.pid(), k++);
			}
			TRACE( std::cout << ul1.to_string() << std::endl; )
			ASSERT(ul1.size() == 15);
		}


		/**
		 * Check that iterators on Bt_list visit all elements
		 */
		chr::Bt_list<int>::iterator test_iterator_move_constructor(chr::Bt_list<int>::iterator it)
		{
			return it;
		}

		void iterators()
		{
			using namespace chr;
			Bt_list<int> ul1;
			std::set< int > ids;

			for (int i=0; i < 10; ++i)
			{
				ul1.insert(i);
				ids.insert(i);
			}
			TRACE( std::cout << ul1.to_string() << std::endl; )

			auto it = ul1.begin();
			std::set< int > ids_copy = ids;
			for ( ; !it.at_end(); ++it)
				ids_copy.erase(*it);
			ASSERT( ids_copy.empty() );

			Bt_list<int> ul2;

			for (int i=0; i < 10; ++i)
				ul2.insert(i);
			TRACE( std::cout << ul2.to_string() << std::endl; )

			auto it2 = ul2.begin();
			std::set< int > ids_copy2 = ids;
			int idx = 0;
			while (!it2.at_end())
			{
				if ((idx % 2) == 0)
				{
					ids_copy2.erase(*it2);
					it2.lock();
					it2.remove();
					it2.next_and_unlock();
					ASSERT(it2.at_end() || (it2.status() == Bt_list<int>::ALIVE));
				} else {
					++it2;
				}
				++idx;
			}
			ASSERT( ul2.size() == 5);
			TRACE( std::cout << "half list: " << ul2.to_string() << std::endl; )

			it2 = ul2.begin();
			while (!it2.at_end())
			{
				ASSERT(it2.status() != Bt_list<int>::REMOVE);
				++it2;
			}
			ASSERT( ids_copy2.size() == ul2.size() );

			auto& ul3 = ul2;
			auto it3 = ul3.begin();
			while (!it3.at_end())
			{
				ASSERT(it3.status() != Bt_list<int>::REMOVE);
				++it3;
			}
			ASSERT( ids_copy2.size() == ul3.size() );

			Bt_list<int>::iterator it4 = ul2.begin();
			ASSERT((*it4) == 8);

			Bt_list<int>::iterator it5 = test_iterator_move_constructor(Bt_list<int>().begin());
			ASSERT(it5.at_end());

			// Empty the list in two steps
			std::vector< Bt_list<int>::iterator > v_iterators;
			it2 = ul2.begin();
			while (!it2.at_end())
			{
				it2.lock();
				it2.remove();
				ASSERT(it2.status() == Bt_list<int>::REMOVE);
				v_iterators.push_back(it2);
				// We don't release yet: it2.next_and_unlock();
				++it2;
			}
			// Count nb empty
			unsigned int nb_empty = 0;
			Bt_list<int>::PID_t p = ul2._first_free;
			while (p != Bt_list<int>::END_LIST)
			{
				++nb_empty;
				p = ul2.data(p)._prev;
			}
			TRACE( std::cout << ul2.to_string(true) << std::endl; )
			ASSERT(nb_empty == 5);

			// We finish to empty the list
			auto prem = v_iterators[0];
			v_iterators.erase(v_iterators.begin());
			for (auto i : v_iterators)
				i.next_and_unlock();

			// Count nb empty
			nb_empty = 0;
			p = ul2._first_free;
			while (p != Bt_list<int>::END_LIST)
			{
				++nb_empty;
				p = ul2.data(p)._prev;
			}
			TRACE( std::cout << ul2.to_string(true) << std::endl; )
			ASSERT(nb_empty == 5);
			prem.next_and_unlock();

			// Count nb empty
			nb_empty = 0;
			p = ul2._first_free;
			while (p != Bt_list<int>::END_LIST)
			{
				++nb_empty;
				p = ul2.data(p)._prev;
			}
			TRACE( std::cout << ul2.to_string(true) << std::endl; )
			ASSERT(nb_empty == 10);
			ASSERT(ul2.empty());
		}

		/**
		 * Test some inserts, removes and callback calls in Bt_list with safe_delete
		 */
		void safe_del_tests()
		{
			using namespace chr;
			Bt_list<int,true,true> ul1;
			std::vector< Bt_list<int,true,true>::iterator > v_iterators;

			ASSERT(ul1.empty());
			for (int i=0; i < 10; ++i)
			{
				auto it = ul1.insert(i);
				ASSERT(*it == i);
				if ((i%2) == 0)
				{
					v_iterators.push_back(it);
				}
			}

			TRACE( std::cout << ul1.to_string() << std::endl; )
			ASSERT(ul1.size() == 10);

			for (auto i : v_iterators)
			{
				i.remove();
			}
			TRACE( std::cout << ul1.to_string() << std::endl; )
			ASSERT(ul1.size() == 5);

			for (int i=20; i < 23; ++i)
			{
				ul1.insert(i);
			}
			TRACE( std::cout << ul1.to_string() << std::endl; )
			ASSERT(ul1.size() == 8);

			// Test if it is safe
			TRACE( std::cout << "Test safe when all empty and not locked iterator" << std::endl; )
			v_iterators.clear();
			auto it1 = ul1.begin();
			while (!it1.at_end())
			{
				v_iterators.push_back(it1);
				++it1;
			}
			ASSERT(!ul1.safe_delete());
			TRACE( std::cout << "Safe status: " << ul1.safe_delete() << std::endl; )
			TRACE( std::cout << "Remove all from list" << std::endl; )
			for (auto i : v_iterators)
				i.remove();
			TRACE( std::cout << ul1.to_string() << std::endl; )
			TRACE( std::cout << "Safe status: " << ul1.safe_delete() << std::endl; )
			ASSERT(ul1.empty());
			ASSERT(ul1.safe_delete());

			TRACE( std::cout << "Test safe when all empty and one locked iterator" << std::endl; )
			for (int i=30; i < 33; ++i)
			{
				ul1.insert(i);
			}
			TRACE( std::cout << ul1.to_string() << std::endl; )
			ASSERT(ul1.size() == 3);
			v_iterators.clear();
			auto it2 = ul1.begin();
			while (!it2.at_end())
			{
				v_iterators.push_back(it2);
				++it2;
			}
			ASSERT(!ul1.safe_delete());
			TRACE( std::cout << "Safe status: " << ul1.safe_delete() << std::endl; )
			TRACE( std::cout << "Remove all from list" << std::endl; )
			v_iterators[0].lock();
			for (auto i : v_iterators)
				i.remove();
			TRACE( std::cout << ul1.to_string() << std::endl; )
			TRACE( std::cout << "Safe status: " << ul1.safe_delete() << std::endl; )
			ASSERT(ul1.empty());
			ASSERT(!ul1.safe_delete());

			v_iterators[0].next_and_unlock();
			TRACE( std::cout << "Safe status: " << ul1.safe_delete() << std::endl; )
			ASSERT(ul1.safe_delete());
		}

		/**
		 * Backtrack tests on Bt_list
		 */
		struct Bt_wrapper : public chr::Backtrack_observer
		{
			chr::Bt_list< int > _list;
			virtual ~Bt_wrapper() {}
			bool rewind(chr::Depth_t previous_depth, chr::Depth_t new_depth) override
			{
				return _list.rewind(previous_depth,new_depth);
			}
		};

		void backtrack_tests()
		{
			using namespace chr;
			std::string str1, str2, str3;
			auto shared_ul1 = chr::make_shared< Bt_wrapper >();
			auto* ul1 = &shared_ul1->_list;

			TRACE( std::cout << "* 1 *******************" << std::endl; )
			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			Depth_t d = Backtrack::depth();
			Backtrack::schedule(shared_ul1._ptr);
			ul1->insert(1);
			TRACE( std::cout << "Initial: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 1);
			str1 = ul1->to_string(1);

			Backtrack::inc_backtrack_depth();
			ul1->insert(2);
			auto it = ul1->begin();
			ul1->remove(it);
			TRACE( std::cout << "New choice insert and remove: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 1);

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 1);
			ASSERT(str1 == ul1->to_string(1));

			TRACE(
				std::cout << "Content :";
				for ( auto it = ul1->begin() ; !it.at_end(); ++it)
					std::cout << " " << *it;
				std::cout << std::endl;
			)

			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			d = Backtrack::depth();
			Backtrack::schedule(shared_ul1._ptr);
			TRACE( std::cout << "Initial: " << ul1->to_string() << std::endl; )
			ul1->insert(2);
			TRACE( std::cout << "Insert: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 2);
			str1 = ul1->to_string(1);

			Backtrack::inc_backtrack_depth();
			it = ul1->begin();
			++it;
			ul1->remove(it);
			it = ul1->begin();
			ul1->remove(it);
			TRACE( std::cout << "New choice remove: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 0);

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 2);
			ASSERT(str1 == ul1->to_string(1));

			TRACE(
				std::cout << "Content :";
				for ( auto it = ul1->begin() ; !it.at_end(); ++it)
					std::cout << " " << *it;
				std::cout << std::endl;
			)
			// Clear the list
			it = ul1->begin();
			it = ul1->remove(it);
			ul1->remove(it);

			TRACE( std::cout << "* 2 *******************" << std::endl; )
			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			d = Backtrack::depth();
			Backtrack::schedule(shared_ul1._ptr);
			for (int i=0; i < 10; ++i)
				ul1->insert(i);
			TRACE( std::cout << "Initial: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			str1 = ul1->to_string(1);

			Backtrack::inc_backtrack_depth();
			for (int i=10; i < 20; ++i)
				ul1->insert(i);
			TRACE( std::cout << "New choice: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 20);

			TRACE( std::cout << "Content :"; )
			it = ul1->begin();
			int idx = 0;
			for ( ; !it.at_end(); ++idx)
			{
				std::cout << " " << *it;
				if ((idx % 2) == 0)
				{
					it = ul1->remove(it);
				} else {
					++it;
				}
			}
			TRACE( std::cout << std::endl; )
			TRACE( std::cout << "Delete: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << *it;
				std::cout << std::endl;
			)

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			ASSERT(str1 == ul1->to_string(1));

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << *it;
				std::cout << std::endl;
			)

			TRACE( std::cout << "* 3 *******************" << std::endl; )
			ASSERT(Backtrack::depth() == 0);
			Backtrack::schedule(shared_ul1._ptr);
			Backtrack::inc_backtrack_depth();
			it = ul1->begin();
			idx = 0;
			for ( ; !it.at_end(); ++idx)
				if ((idx % 2) == 0)
				{
					it = ul1->remove(it);
				} else {
					++it;
				}
			TRACE( std::cout << "New choice Delete: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 5);
			str2 = ul1->to_string(1);

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << *it;
				std::cout << std::endl;
			)

			Backtrack::inc_backtrack_depth();
			it = ul1->begin();
			it.remove();
			TRACE( std::cout << "New choice remove first: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 4);

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << *it;
				std::cout << std::endl;
			)

			for (int i=10; i < 20; ++i)
				ul1->insert(i);
			TRACE( std::cout << "Then add: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 14);

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << *it;
				std::cout << std::endl;
			)

			Backtrack::back_to(d+1);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 5);
			ASSERT(str2 == ul1->to_string(1));

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << *it;
				std::cout << std::endl;
			)

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			ASSERT(str1 == ul1->to_string(1));

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << *it;
				std::cout << std::endl;
			)

			TRACE( std::cout << "* 4 *******************" << std::endl; )
			TRACE( std::cout << "Start empty" << std::endl; )
			ASSERT(Backtrack::depth() == 0);
			auto shared_ul2 = chr::make_shared< Bt_wrapper >();
			auto* ul2 = &shared_ul2->_list;

			Backtrack::schedule(shared_ul2._ptr);
			d = Backtrack::depth();
			ASSERT(d == 0);
			str1 = ul2->to_string(1);

			Backtrack::inc_backtrack_depth();
			for (int i=1; i < 11; ++i)
				ul2->insert(i);

			auto it2 = ul2->begin();
			TRACE(
				std::cout << "New choice add: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << *it2;
				std::cout << std::endl;
			)
			str2 = ul2->to_string(1);

			Backtrack::inc_backtrack_depth();
			it2 = ul2->begin();
			it2.remove();

			TRACE(
				std::cout << "New choice kill: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << *it2;
				std::cout << std::endl;
			)
			str3 = ul2->to_string(1);

			Backtrack::inc_backtrack_depth();
			it2 = ul2->begin();
			++it2;
			it2.remove();

			TRACE(
				std::cout << "New choice kill again: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << *it2;
				std::cout << std::endl;
			)

			it2 = ul2->begin();
			++it2;
			it2.remove();

			TRACE(
				std::cout << "kill again again: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << *it2;
				std::cout << std::endl;
			)

			ul2->insert(20);
			TRACE(
				std::cout << "Then add: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << *it2;
				std::cout << std::endl;
			)

			Backtrack::back_to(d+2);
			TRACE(
				std::cout << "Back to previous: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << *it2;
				std::cout << std::endl;
			)
			ASSERT(ul2->to_string(1) == str3);

			Backtrack::back_to(d+1);
			TRACE(
				std::cout << "Back to previous: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << *it2;
				std::cout << std::endl;
			)
			ASSERT(ul2->to_string(1) == str2);

			Backtrack::back_to(d);
			TRACE(
				std::cout << "Back to first: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << *it2;
				std::cout << std::endl;
			)
			ASSERT(ul2->to_string(1) == str1);

			// Test that the number of observers is restored after backtrack
			ASSERT(!ul1->empty());
			it = ul1->begin();
			ASSERT(ul1->data(it.pid())._obs_count == 0);
			it.lock();
			TRACE( std::cout << "(ul1) Lock first item: " << ul1->to_string(1) << std::endl; )
			ASSERT(ul1->data(it.pid())._obs_count == 1);

			Backtrack::inc_backtrack_depth();
			it.unlock();
			TRACE( std::cout << "New choice and unlock first item: " << ul1->to_string(1) << std::endl; )
			ASSERT(ul1->data(it.pid())._obs_count == 1); // Is a backtrackable item, do obs_count doesn't change

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to first: " << ul1->to_string(1) << std::endl; )
			ASSERT(ul1->data(it.pid())._obs_count == 1);
		}

		/**
		 * Backtrack persistent tests on Bt_list
		 */
		struct Bt_wrapper_persistent : public chr::Backtrack_observer
		{
			chr::Bt_list< int, false > _list;
			virtual ~Bt_wrapper_persistent() {}
			bool rewind(chr::Depth_t, chr::Depth_t) override
			{
				return false;
			}
		};

		void backtrack_persistent_tests()
		{
			using namespace chr;
			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			std::string str1, str2, str3;
			auto shared_ul1 = chr::make_shared< Bt_wrapper_persistent >();
			auto* ul1 = &shared_ul1->_list;
			Backtrack::schedule(shared_ul1._ptr);

			TRACE( std::cout << "* 1 *******************" << std::endl; )
			Depth_t d = Backtrack::depth();
			for (int i=0; i < 10; ++i)
				ul1->insert(i);
			TRACE( std::cout << "Initial: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			str1 = ul1->to_string(1);

			Backtrack::inc_backtrack_depth();
			for (int i=10; i < 20; ++i)
				ul1->insert(i);
			TRACE( std::cout << "New choice: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 20);
			str2 = ul1->to_string(1);

			Backtrack::inc_backtrack_depth();
			auto it = ul1->begin();
			int idx = 0;
			for ( ; !it.at_end(); ++idx)
			{
				if ((idx % 2) == 0)
				{
					it = ul1->remove(it);
				} else {
					++it;
				}
			}
			str3 = ul1->to_string(1);
			TRACE( std::cout << "Delete: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << *it;
				std::cout << std::endl;
			)

			Backtrack::back_to(d+1);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			ASSERT(str3 == ul1->to_string(1));

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			ASSERT(str3 == ul1->to_string(1));
		}

		/**
		 * OtherbBacktrack tests on Bt_list (with replace() operator)
		 */
		struct Bt_wrapper_replace : public chr::Backtrack_observer
		{
			chr::Bt_list< int > _list;
			virtual ~Bt_wrapper_replace() {}
			bool rewind(chr::Depth_t previous_depth, chr::Depth_t new_depth) override
			{
				return _list.rewind(previous_depth,new_depth);
			}
		};

		void replace_backtrack_tests()
		{
			using namespace chr;
			std::string str1, str2, str3;
			auto shared_ul1 = chr::make_shared< Bt_wrapper_replace >();
			auto* ul1 = &shared_ul1->_list;

			TRACE( std::cout << "* 1 *******************" << std::endl; )
			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			Depth_t d = Backtrack::depth();
			Backtrack::schedule(shared_ul1._ptr);
			ul1->insert(1);
			TRACE( std::cout << "Initial: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 1);
			str1 = ul1->to_string(1);

			Backtrack::inc_backtrack_depth();
			ul1->insert(2);
			auto it = ul1->begin();
			ul1->remove(it);
			TRACE( std::cout << "New choice insert and remove: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 1);

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 1);
			ASSERT(str1 == ul1->to_string(1));

			TRACE(
				std::cout << "Content :";
				for ( auto it = ul1->begin() ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)

			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			d = Backtrack::depth();
			Backtrack::schedule(shared_ul1._ptr);
			TRACE( std::cout << "Initial: " << ul1->to_string() << std::endl; )
			ul1->insert(2);
			TRACE( std::cout << "Insert: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 2);
			str1 = ul1->to_string(1);

			Backtrack::inc_backtrack_depth();
			it = ul1->begin();
			++it;
			ul1->remove(it);
			it = ul1->begin();
			ul1->remove(it);
			TRACE( std::cout << "New choice remove: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 0);

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 2);
			ASSERT(str1 == ul1->to_string(1));

			TRACE(
				std::cout << "Content :";
				for ( auto it = ul1->begin() ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)
			// Clear the list
			it = ul1->begin();
			it = ul1->remove(it);
			ul1->remove(it);

			TRACE( std::cout << "* 2 *******************" << std::endl; )
			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			d = Backtrack::depth();
			Backtrack::schedule(shared_ul1._ptr);
			for (int i=0; i < 10; ++i)
				ul1->insert(i);
			TRACE( std::cout << "Initial: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			str1 = ul1->to_string(1);

			Backtrack::inc_backtrack_depth();
			for (int i=10; i < 20; ++i)
				ul1->insert(i);
			TRACE( std::cout << "New choice: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 20);

			TRACE( std::cout << "Content :"; )
			it = ul1->begin();
			int idx = 0;
			for ( ; !it.at_end(); ++idx)
			{
				std::cout << " " << (*it);
				if ((idx % 2) == 0)
				{
					it = ul1->remove(it);
				} else {
					++it;
				}
			}
			TRACE( std::cout << std::endl; )
			TRACE( std::cout << "Delete: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			ASSERT(str1 == ul1->to_string(1));

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)

			TRACE( std::cout << "* 3 *******************" << std::endl; )
			ASSERT(Backtrack::depth() == 0);
			Backtrack::schedule(shared_ul1._ptr);
			Backtrack::inc_backtrack_depth();
			it = ul1->begin();
			idx = 0;
			for ( ; !it.at_end(); ++idx)
				if ((idx % 2) == 0)
				{
					it = ul1->remove(it);
				} else {
					++it;
				}
			TRACE( std::cout << "New choice Delete: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 5);
			str2 = ul1->to_string(1);

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)

			Backtrack::inc_backtrack_depth();
			it = ul1->begin();
			it.remove();
			TRACE( std::cout << "New choice remove first: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 4);

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)

			for (int i=10; i < 20; ++i)
				ul1->insert(i);
			TRACE( std::cout << "Then add: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 14);

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)

			Backtrack::back_to(d+1);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 5);
			ASSERT(str2 == ul1->to_string(1));

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			ASSERT(str1 == ul1->to_string(1));

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)

			TRACE( std::cout << "* 4 *******************" << std::endl; )
			TRACE( std::cout << "Start empty" << std::endl; )
			ASSERT(Backtrack::depth() == 0);
			auto shared_ul2 = chr::make_shared< Bt_wrapper_replace >();
			auto* ul2 = &shared_ul2->_list;

			Backtrack::schedule(shared_ul2._ptr);
			d = Backtrack::depth();
			ASSERT(d == 0);
			str1 = ul2->to_string(1);

			Backtrack::inc_backtrack_depth();
			for (int i=1; i < 11; ++i)
				ul2->insert(i);

			auto it2 = ul2->begin();
			TRACE(
				std::cout << "New choice add: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << (*it2);
				std::cout << std::endl;
			)
			str2 = ul2->to_string(1);

			Backtrack::inc_backtrack_depth();
			it2 = ul2->begin();
			it2.remove();

			TRACE(
				std::cout << "New choice kill: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << (*it2);
				std::cout << std::endl;
			)
			str3 = ul2->to_string(1);

			Backtrack::inc_backtrack_depth();
			it2 = ul2->begin();
			++it2;
			it2.remove();

			TRACE(
				std::cout << "New choice kill again: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << (*it2);
				std::cout << std::endl;
			)

			it2 = ul2->begin();
			++it2;
			it2.remove();

			TRACE(
				std::cout << "kill again again: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << (*it2);
				std::cout << std::endl;
			)

			ul2->insert(20);
			TRACE(
				std::cout << "Then add: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << (*it2);
				std::cout << std::endl;
			)

			Backtrack::back_to(d+2);
			TRACE(
				std::cout << "Back to previous: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << (*it2);
				std::cout << std::endl;
			)
			ASSERT(ul2->to_string(1) == str3);

			Backtrack::back_to(d+1);
			TRACE(
				std::cout << "Back to previous: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << (*it2);
				std::cout << std::endl;
			)
			ASSERT(ul2->to_string(1) == str2);

			Backtrack::back_to(d);
			TRACE(
				std::cout << "Back to first: " << ul2->to_string() << std::endl;
				std::cout << "Content :";
				it2 = ul2->begin();
				for ( ; !it2.at_end(); ++it2)
					std::cout << " " << (*it2);
				std::cout << std::endl;
			)
			ASSERT(ul2->to_string(1) == str1);

			// Test that the number of observers is restored after backtrack
			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			d = Backtrack::depth();
			Backtrack::schedule(shared_ul1._ptr);
			ASSERT(!ul1->empty());
			it = ul1->begin();
			ASSERT(ul1->data(it.pid())._obs_count == 0);
			it.lock();
			TRACE( std::cout << "(ul1) Lock first item: " << ul1->to_string(1) << std::endl; )
			ASSERT(ul1->data(it.pid())._obs_count == 1);

			Backtrack::inc_backtrack_depth();
			it.unlock();
			TRACE( std::cout << "New choice and unlock first item: " << ul1->to_string(1) << std::endl; )
			ASSERT(ul1->data(it.pid())._obs_count == 1); // Is a backtrackable item, do obs_count doesn't change

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to first: " << ul1->to_string(1) << std::endl; )
			ASSERT(ul1->data(it.pid())._obs_count == 1);


			// Backtrack tests with replace
			TRACE( std::cout << "* 5 *******************" << std::endl; )
			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			d = Backtrack::depth();
			Backtrack::schedule(shared_ul1._ptr);
			TRACE( std::cout << "Initial: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			str1 = ul1->to_string(1);

			Backtrack::inc_backtrack_depth();
			it = ul1->begin();
			idx = 0;
			for ( ; !it.at_end(); ++it, ++idx)
				if ((idx % 2) == 0)
					ul1->replace(it,100+idx);
			TRACE( std::cout << "New replacements: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			ASSERT(str1 == ul1->to_string(1));

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)

			TRACE( std::cout << "* 6 *******************" << std::endl; )
			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			d = Backtrack::depth();
			Backtrack::schedule(shared_ul1._ptr);
			TRACE( std::cout << "Initial: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			str1 = ul1->to_string(1);

			Backtrack::inc_backtrack_depth();
			it = ul1->begin();
			idx = 1;
			++it;
			for ( ; !it.at_end(); ++it, ++idx)
				if ((idx % 2) == 0)
					ul1->replace(it,100+idx);
			TRACE( std::cout << "New replacements: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to previous: " << ul1->to_string() << std::endl; )
			ASSERT(ul1->size() == 10);
			ASSERT(str1 == ul1->to_string(1));

			TRACE(
				std::cout << "Content :";
				it = ul1->begin();
				for ( ; !it.at_end(); ++it)
					std::cout << " " << (*it);
				std::cout << std::endl;
			)
		}


	}

	void run_bt_list_tests(cute::suite& s)
	{
		s.push_back( { "Bt_list, insert and remove tests", bt_list::insert_remove_tests } );
		s.push_back( { "Bt_list, replace tests", bt_list::replace_tests } );
		s.push_back( { "Bt_list iterator", bt_list::iterators } );
		s.push_back( { "Bt_list safe_del", bt_list::safe_del_tests } );
		s.push_back( { "Bt_list backtrack tests", bt_list::backtrack_tests } );
		s.push_back( { "Bt_list backtrack (persistent) tests", bt_list::backtrack_persistent_tests } );
		s.push_back( { "Bt_list, replace backtrack tests", bt_list::replace_backtrack_tests } );
	}
}
