/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#ifndef PARSING_TESTS_HH_
#define PARSING_TESTS_HH_

#include <cute.h>
#include <cute_runner.h>

#ifndef TESTS_PATH
	#define TESTS_PATH "./"
#endif

namespace tests {
	namespace compiler {

	/**
	 * Array of names of compiler tests
	 */
	constexpr const char * g_compiler_tests_suite[] = {
			"no_rule",
			"propagation_noh_1",
			"constraint_decl_1", "constraint_decl_2", "constraint_decl_3", "constraint_decl_4", "constraint_decl_5",
			"no_head", "head_1", "head_2", "head_3", "head_4", "head_5", "head_6", "head_7", "head_8", "head_9", "head_10", "head_11",
			"no_guard", "guard_1", "guard_2", "guard_3", "guard_4", "guard_5", "guard_6", "guard_7", "guard_8",
			"no_body", "body_1", "body_2", "body_3", "body_4", "body_5", "body_6", "body_7", "body_8",
			"unify_1", "unify_2", "unify_3", "unify_4", "unify_5",
			"exists_1", "exists_2", "exists_3", "exists_4", "exists_5", "exists_6", "exists_7", "exists_8", "exists_9", "exists_10", "exists_11", "exists_12",
			"exists_13", "exists_14", "exists_15", "exists_16", "exists_17",
			"forall_1", "forall_2",
			"try_1", "try_2",
			"chr_count_1", "chr_count_2", "chr_count_3",
			"pragma_1", "pragma_2", "pragma_3", "pragma_4", "pragma_5", "pragma_6", "pragma_7", "pragma_8", "pragma_9",
			"operator_1", "operator_2", "operator_3", "operator_4", "operator_5", "operator_6", "operator_7", "operator_8", "operator_9",
			"operator_10", "operator_11", "operator_12", "operator_13", "operator_14", "operator_15", "operator_16", "operator_17",
			"operator_20", "operator_21", "operator_22", "operator_23", "operator_24", "operator_25", "operator_26", "operator_27", "operator_28", "operator_29",
			"operator_30", "operator_31", "operator_32", "operator_33", "operator_34", "operator_35", "operator_36", "operator_37", "operator_38", "operator_39",
			"operator_40", "operator_41", "operator_42", "operator_43", "operator_44",
			"chr_constraint_no_arg", "chr_constraint_cpp_keyword", "undeclared_variable", "unused_rule_1", "global_variable_1", "global_variable_2", "global_variable_3", "global_variable_4",
			"local_variable_1", "local_variable_2",
			"gcd"
	};

	constexpr int g_compiler_tests_size = sizeof(g_compiler_tests_suite)/sizeof(char *); ///< Statically computed number of compiler tests

	/**
	 * Represent a test for the CHR compiler. The template
	 * parameter num_test represent the i^th slot of the g_compiler_tests_suite
	 * variable.
	 */
	template < unsigned int num_test >
	struct compiler_parsing_test
	{
		static constexpr const char * name = g_compiler_tests_suite[num_test];	///< Name of test

		/**
		 * Create and run the test
		 */
		static void create();
	};

	}
}

#include <parsing_tests.hpp>

#endif /* PARSING_TESTS_HH_ */
