/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <array>
#include <fstream>
#include <sstream>
#include <string>
#include <cerrno>

#include <runtime_tests.hh>
#include <chrpp.hh>

namespace tests {
	namespace logical_var {

		/**
		 * Just simple tests over Logical_var
		 */
		void simple_logical_var()
		{
			chr::Logical_var< int > X, Y;
			ASSERT(X %= Y);
			ASSERT(X == Y);

			chr::Logical_var< int > Z(X);
			ASSERT((X == Z) && (Y == Z));
			ASSERT(!X.ground() && !Y.ground() && !Z.ground());

			chr::Logical_var< int > W(3);
			ASSERT(Y != W);
			ASSERT(Y %= W);
			ASSERT(X.ground() && Y.ground() && Z.ground() && W.ground());
			ASSERT((*X == *Z) && (*X == *Y) && (*Z == *W) && (*X == 3));
			ASSERT((X == Z) && (X == Y) && (Z == W) && (W == 3));

			chr::Logical_var< int > V(2);
			ASSERT(!(V %= W));
			ASSERT((V != W) && (V != 3) && (V == 2) && (*V == 2));
		}

		/**
		 * Just simple tests over Logical_var_ground
		 */
		void simple_logical_var_ground()
		{
			chr::Logical_var_ground< int > X(3), Y(3);
			ASSERT(X %= Y);
			ASSERT(X == Y);

			chr::Logical_var_ground< int > Z(X);
			ASSERT((X == Z) && (Y == Z));

			chr::Logical_var_ground< int > W(2);
			ASSERT(!(Y %= W));
			ASSERT(X.ground() && Y.ground() && Z.ground() && W.ground());
			ASSERT((*X == *Z) && (*X == *Y) && (*Z != *W) && (*X == 3));
			ASSERT((X == Z) && (X == Y) && (Z != W) && (W == 2) && (X == 3));
		}

		/**
		 * Check that Logical_var and Logical_var_ground are well connected
		 */
		void logical_var_and_logical_var_ground()
		{
			chr::Logical_var< int > X;
			chr::Logical_var_ground< int > Y(2);
			ASSERT(X != Y);
			ASSERT(!(Y == X));
			ASSERT(Y != X);
			ASSERT(X %= Y);
			ASSERT((X == Y) && (Y == X) && (*X == *Y));

			chr::Logical_var< int > Z, V(Z);
			chr::Logical_var_ground< int > W(2);
			ASSERT(W %= Z);
			ASSERT((X == Z) && (*W == *Z) && (V == W) && (*V == *W));
		}

		void misc()
		{
			chr::Logical_var< int > X;
			chr::Logical_var_ground< int > Y(2);
			chr::Logical_var< unsigned int > Z;
			chr::Logical_var_ground< unsigned int > W(3);

			ASSERT(X != Y);
			X %= 3;
			Z %= 3;
			ASSERT((int)*Z == *X);
			ASSERT(Y != (int)*W);
		}

		/**
		 * Just simple backtrack tests on Logical_variable
		 */
		void backtrack_tests()
		{
			using namespace chr;
			Logical_var< int > X, Y, Z;

			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			TRACE( std::cout << "X=" << X << ", Y=" << Y << ", Z=" << Z << std::endl; )
			Depth_t d = Backtrack::depth();
			Backtrack::inc_backtrack_depth();
			X %= Y;
			TRACE( std::cout << "New choice X %= Y: " << X << " " << Y << std::endl; )
			ASSERT(X == Y);
			ASSERT(X._var_imp->_previous_snapshot);
			ASSERT(Y._var_imp->_previous_snapshot);
			ASSERT(Backtrack::callback_count() == 2);

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to X != Y: " << X << " " << Y << std::endl; )
			ASSERT(X != Y);
			ASSERT(!X._var_imp->_previous_snapshot);
			ASSERT(!Y._var_imp->_previous_snapshot);
			ASSERT(Backtrack::callback_count() == 0);

			Backtrack::inc_backtrack_depth();
			X %= Z;
			TRACE( std::cout << "New choice X %= Z: " << X << " " << Z << std::endl; )
			ASSERT(X == Z);
			ASSERT(X._var_imp->_previous_snapshot);
			ASSERT(Z._var_imp->_previous_snapshot);
			ASSERT(Backtrack::callback_count() == 2);

			Backtrack::inc_backtrack_depth();
			Z %= 2;
			TRACE( std::cout << "New choice X == Z == 2: " << X << " " << Z << std::endl; )
			ASSERT(X == 2);
			ASSERT(Backtrack::callback_count() == 2);

			Backtrack::back_to(d+1);
			TRACE( std::cout << "Back to X == Z != 2: " << X << " " << Z << std::endl; )
			ASSERT(X != 2);
			ASSERT(Z != 2);
			ASSERT(Backtrack::callback_count() == 2);

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to X != Z: " << X << " " << Z << std::endl; )
			ASSERT(X != Z);
			ASSERT(!X._var_imp->_previous_snapshot);
			ASSERT(!Z._var_imp->_previous_snapshot);
			ASSERT(Backtrack::callback_count() == 0);

			Backtrack::inc_backtrack_depth();
			Y %= Z;
			TRACE( std::cout << "New choice Y %= Z: " << Y << " " << Z << std::endl; )
			ASSERT(Y == Z);
			ASSERT(Y._var_imp->_previous_snapshot);
			ASSERT(Z._var_imp->_previous_snapshot);
			ASSERT(Backtrack::callback_count() == 2);

			Backtrack::inc_backtrack_depth();
			Z %= 2;
			TRACE( std::cout << "New choice Y == Z == 2: " << Y << " " << Z << std::endl; )
			ASSERT(Y == 2);
			ASSERT(Backtrack::callback_count() == 2);

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to Y != Z: " << Y << " " << Z << std::endl; )
			ASSERT(Y != Z);
			ASSERT(!Y._var_imp->_previous_snapshot);
			ASSERT(!Z._var_imp->_previous_snapshot);
			ASSERT(Backtrack::callback_count() == 0);
		}
	}

	void run_logical_var_tests(cute::suite& s)
	{
		s.push_back( { "Simple tests on Logical_var", logical_var::simple_logical_var } );
		s.push_back( { "Simple tests on Logical_var_ground", logical_var::simple_logical_var_ground } );
		s.push_back( { "More tests on Logical_var and Logical_var_ground", logical_var::logical_var_and_logical_var_ground } );
		s.push_back( { "Yet more tests on Logical_var", logical_var::misc } );
		s.push_back( { "Backtrack tests on Logical_var", logical_var::backtrack_tests } );
	}
}
