/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <parsing_tests.hh>
#include <runtime_tests.hh>
#include <chr_test_problems.hh>

#include <cute.h>
#include <ide_listener.h>
#include <xml_listener.h>
#include <cute_runner.h>

#include <../chrppc/constants.hh>

/**
 * Tests schedule unroller
 * Static template loop to create and load tests if their name is
 * stored in a static array. The loop is unfold and tests recorded
 * in a CUTE test suite.
 */
template<int Begin, int End, int Step, template < unsigned int value > class T >
struct Unroller_tests {
	/**
	 * Create and load the i^th test related to the i^th iteration number
	 * @param s The test suite to use
	 */
	static void step(cute::suite& s) {
		s.push_back( { T< Begin >::name, T< Begin >::create } );
		Unroller_tests<Begin+Step, End, Step, T>::step(s);
	}
};

/**
 * Tests schedule unroller end
 */
template< int End, int Step, template < unsigned int value > class T >
struct Unroller_tests< End, End, Step, T > {
	static void step(cute::suite&) { }
};

/**
 * Run all parsing tests problem (build a test suite)
 * @param argc Console program parameters count
 * @param argv Console program parameters
 * @return True if all test passed, False otherwise
 */
bool run_parsing_tests(int argc, char const *argv[]) {
	cute::suite s { };
	// Compiler parsing test suite
	Unroller_tests< 0, tests::compiler::g_compiler_tests_size, 1, tests::compiler::compiler_parsing_test >::step(s);
	cute::xml_file_opener xmlfile(argc, argv);
	cute::xml_listener<cute::ide_listener<>> lis(xmlfile.out);
	auto runner = cute::makeRunner(lis, argc, argv);
	bool success = runner(s, "parsing_tests");
	return success;
}

/**
 * Run all parsing tests problem (build a test suite)
 * @param argc Console program parameters count
 * @param argv Console program parameters
 * @return True if all test passed, False otherwise
 */
bool run_runtime_tests(int argc, char const *argv[]) {
	cute::suite s { };
	// Compiler parsing test suite
	cute::xml_file_opener xmlfile(argc, argv);
	cute::xml_listener<cute::ide_listener<>> lis(xmlfile.out);
	TRACE( chr::Log::set_output( std::cout ); )
//	TRACE( chr::Log::register_flags(chr::Log::ALL); )
	tests::run_bt_list_tests(s);
	tests::run_logical_var_tests(s);
	tests::run_constraint_store_tests(s);
	tests::run_history_tests(s);
	tests::run_advanced_backtrack_tests(s);
	tests::run_bt_interval_tests(s);
	auto runner = cute::makeRunner(lis, argc, argv);
	bool success = runner(s, "runtime_tests");
	return success;
}

/**
 * Run all simple test problems (build a test suite)
 * @param argc Console program parameters count
 * @param argv Console program parameters
 * @return True if all test passed, False otherwise
 */
bool run_small_test_problems(int argc, char const *argv[]) {
	cute::suite s { };
	// Compiler parsing test suite
	cute::xml_file_opener xmlfile(argc, argv);
	cute::xml_listener<cute::ide_listener<>> lis(xmlfile.out);
	TRACE( chr::Log::set_output( std::cout ); )
//	TRACE( chr::Log::register_flags(chr::Log::ALL); )
	tests::run_chr_include_problems(s);
	tests::run_index_generation_problem(s);
	tests::run_local_variable_declaration_problem(s);
	tests::run_constraint_wake_up_tests(s);
	tests::run_mutable_variable_tests(s);
	tests::run_advanced_history_tests(s);
	tests::run_propagation_noh_problem(s);
	tests::run_never_stored_problem(s);
	tests::run_r_list_problems(s);
	tests::run_chr_count_problems(s);
	tests::run_logical_structure_problems(s);
	tests::run_bang_problem(s);
	auto runner = cute::makeRunner(lis, argc, argv);
	bool success = runner(s, "small_test_problems");
	return success;
}


/**
 * Run all more advanced test problems (build a test suite)
 * @param argc Console program parameters count
 * @param argv Console program parameters
 * @return True if all test passed, False otherwise
 */
bool run_test_problems(int argc, char const *argv[]) {
	cute::suite s { };
	// Compiler parsing test suite
	cute::xml_file_opener xmlfile(argc, argv);
	cute::xml_listener<cute::ide_listener<>> lis(xmlfile.out);
	TRACE( chr::Log::set_output( std::cout ); )
//	TRACE( chr::Log::register_flags(chr::Log::ALL); )
	tests::run_gcd_problems(s);
	tests::run_sum_problems(s);
	tests::run_fib_problems(s);
	tests::run_nqueens_problems(s);
	tests::run_sudoku_problems(s);
	tests::run_nimfibo_problems(s);
	auto runner = cute::makeRunner(lis, argc, argv);
	bool success = runner(s, "test_problems");
	return success;
}

int main(int argc, char const *argv[]) {
	bool ret = run_parsing_tests(argc, argv)
			&& run_runtime_tests(argc, argv)
			&& run_small_test_problems(argc, argv)
			&& run_test_problems(argc, argv);
	if (ret)
		std::cout << "All tests succeeded." << std::endl;
	else
		std::cout << "At least one test failed." << std::endl;
    return (ret ? EXIT_SUCCESS : EXIT_FAILURE);
}
