/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <algorithm>
#include <array>
#include <set>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <cerrno>

#include <trace.hh>
#include <runtime_tests.hh>
#include <chrpp.hh>
#include <bt_interval.hh>

namespace tests {
	namespace bt_interval {
		void basic_tests_range()
		{
			using namespace chr;
			TRACE( std::cout << "*** tests: eq"<< std::endl; )
			Interval<int,true> ul1(1,10);
			ul1.eq(2);
			TRACE( std::cout << "== 2 --> " << ul1.to_string() << std:: endl; )
			ASSERT((ul1.min() == 2) && (ul1.max() == 2));

			Interval<int,true> ul2(1,10);
			ul2.eq(15);
			ASSERT(ul2.empty());

			Interval<int,true> ul3(1,10);
			ul3.eq(5);
			ASSERT((ul3.min() == ul3.max()) && (ul3.min() == 5));

			TRACE( std::cout << "*** tests: lq"<< std::endl; )
			Interval<int,true> ul4(1,10);
			ASSERT(ul4.lq(11) == false);
			ul4.lq(5);
			ASSERT((ul4.min() == 1) && (ul4.max() == 5));
			ul4.lq(0);
			ASSERT(ul4.empty());

			TRACE( std::cout << "*** tests: gq"<< std::endl; )
			Interval<int,true> ul5(1,10);
			ASSERT(ul5.gq(0) == false);
			ul5.gq(5);
			ASSERT((ul5.min() == 5) && (ul5.max() == 10));
			ul5.gq(11);
			ASSERT(ul5.empty());

			TRACE( std::cout << "*** tests: nq"<< std::endl; )
			Interval<int,true> ul6(1,10);
			ASSERT(ul6.nq(0) == false);
			ASSERT(ul6.nq(3) == false);
			ASSERT(ul6.nq(1) == true);
			ASSERT(ul6.nq(10) == true);
			ASSERT(!ul6.in(1));
			ASSERT(!ul6.in(10));

			TRACE( std::cout << "*** tests: in"<< std::endl; )
			Interval<int,true> ul7(1,10);
			ASSERT(ul7.in(0) == false);
			ASSERT(ul7.in(11) == false);
			ASSERT(ul7.in(1));
			ASSERT(ul7.in(9));

			TRACE( std::cout << "*** tests: narrow"<< std::endl; )
			Interval<int,true> ul8(1,10);
			ASSERT(ul8.narrow(Interval<int,true>(0,11)) == false);
			ASSERT(ul8.narrow(Interval<int,true>(0,9)) == true);
			ASSERT(ul8.in(9) == true);
			ASSERT(ul8.in(10) == false);
			ASSERT(ul8.narrow(Interval<int,true>(6,12)) == true);
			ASSERT(ul8.in(6) == true);
			ASSERT(ul8.in(5) == false);
			ASSERT(ul8.narrow(Interval<int,true>(7,8)) == true);
			ASSERT(ul8.in(7) == true);
			ASSERT(ul8.in(8) == true);
			ASSERT(ul8.in(6) == false);
			ASSERT(ul8.in(9) == false);

			TRACE( std::cout << "*** tests: minus"<< std::endl; )
			Interval<int,true> ul9(1,10);
			Interval<int,true> ul_empty(3,3);
			ul_empty.nq(3);
			ASSERT(ul9.minus(Interval<int,true>(3,5)) == false);
			ASSERT(ul9.minus(Interval<int,true>(-1,0)) == false);
			ASSERT(ul9.minus(Interval<int,true>(11,13)) == false);
			ASSERT(ul9.minus(ul_empty) == false);
			ASSERT(ul9.minus(Interval<int,true>(0,3)) == true);
			ASSERT(ul9.in(4) == true);
			ASSERT(ul9.in(3) == false);
			ASSERT(ul9.minus(Interval<int,true>(8,12)) == true);
			ASSERT(ul9.in(7) == true);
			ASSERT(ul9.in(8) == false);
			ASSERT(ul9.minus(Interval<int,true>(3,10)) == true);
			ASSERT(ul9.empty());
		}


		chr::Interval<int> test_move_constructor(chr::Interval<int> l)
		{
			return l;
		}

		/**
		 * Test on chr::Interval basics functions
		 */
		void basic_tests()
		{
			using namespace chr;
			Interval<int> ul = test_move_constructor(Interval<int>(2,2));

			TRACE( std::cout << "*** tests: nq"<< std::endl; )
			Interval<int> ul0(1,1);
			ASSERT((ul0.min() == 1) && (ul0.max() == 1) && ul0.range());
			ul0.nq(1);
			ASSERT(ul0.empty());

			Interval<int> ul1(1,6);
			TRACE( std::cout << ul1.to_string() << std:: endl; )
			ASSERT((ul1.min() == 1) && (ul1.max() == 6) && ul1.range());
			ul1.nq(1);
			TRACE( std::cout << "!= 1 --> " << ul1.to_string() << std:: endl; )
			ASSERT((ul1.min() == 2) && (ul1.max() == 6) && ul1.range());
			ul1.nq(6);
			TRACE( std::cout << "!= 6 --> " << ul1.to_string() << std:: endl; )
			ASSERT((ul1.min() == 2) && (ul1.max() == 5) && ul1.range());

			// Tests not range
			ul1.nq(4);
			ASSERT((ul1.min() == 2) && (ul1.max() == 5) && !ul1.range());
			TRACE( std::cout << "!= 4 --> " << ul1.to_string() << std:: endl; )
			ul1.nq(3);
			TRACE( std::cout << "!= 3 --> " << ul1.to_string() << std:: endl; )
			ul1.nq(5);
			TRACE( std::cout << "!= 5 --> " << ul1.to_string() << std:: endl; )
			ASSERT((ul1.min() == 2) && (ul1.max() == 2) && ul1.range());

			Interval<int> ul2(1,10);
			std::cout << ul2.to_string() << std:: endl;
			for (int i=2; i <= 8; i+=2)
			{
				ul2.nq(i);
			}
			std::string str2 = ul2._range_list.to_string(1); 
			TRACE( std::cout << ul2.to_string() << std:: endl; )
			ASSERT((ul2.min() == 1) && (ul2.max() == 10) && !ul2.range());
			ul2.nq(2);
			ASSERT(str2 == ul2._range_list.to_string(1)); 

			// Tests not range (closer min)
			Interval<int> ul3(1,20);
			ul3.nq(3);
			ul3.nq(7);
			ul3.nq(9);
			TRACE( std::cout << "nq (closer_min) --> " << ul3.to_string() << std:: endl; )
			ASSERT((ul3.min() == 1) && (ul3.max() == 20) && !ul3.range());
			ul3.nq(8);
			TRACE( std::cout << "!= 8 --> " << ul3.to_string() << std:: endl; )
			ul3.nq(4);
			TRACE( std::cout << "!= 4 --> " << ul3.to_string() << std:: endl; )
			ul3.nq(6);
			TRACE( std::cout << "!= 6 --> " << ul3.to_string() << std:: endl; )

			// Tests not range (not closer min)
			Interval<int> ul4(1,20);
			ul4.nq(17);
			ul4.nq(13);
			ul4.nq(11);
			TRACE( std::cout << "nq (not closer_min) --> " << ul4.to_string() << std:: endl; )
			ASSERT((ul4.min() == 1) && (ul4.max() == 20) && !ul4.range());
			ul4.nq(12);
			TRACE( std::cout << "!= 12 --> " << ul4.to_string() << std:: endl; )
			ul4.nq(16);
			TRACE( std::cout << "!= 16 --> " << ul4.to_string() << std:: endl; )
			ul4.nq(14);
			TRACE( std::cout << "!= 14 --> " << ul4.to_string() << std:: endl; )

			TRACE( std::cout << "*** tests: eq"<< std::endl; )
			Interval<int> ul5_0(1,1);
			ASSERT(ul5_0.eq(1) == false);

			Interval<int> ul5_1(1,1);
			ul5_1.eq(2);
			TRACE( std::cout << "== 2 --> " << ul5_1.to_string() << std:: endl; )
			ASSERT(ul5_1.empty());

			Interval<int> ul5_2(1,1);
			ul5_2.eq(0);
			TRACE( std::cout << "== 0 --> " << ul5_2.to_string() << std:: endl; )
			ASSERT(ul5_2.empty());

			Interval<int> ul5_3(1,4);
			ul5_3.eq(2);
			TRACE( std::cout << "== 2 --> " << ul5_3.to_string() << std:: endl; )
			ASSERT((ul5_3.min() == 2) && (ul5_3.max() == 2) && ul5_3.range());

			TRACE( std::cout << "*** tests: eq (not range)"<< std::endl; )
			Interval<int> ul5_4(1,10);
			ul5_4.nq(4);
			ul5_4.eq(2);
			TRACE( std::cout << "== 2 --> " << ul5_4.to_string() << std:: endl; )
			ASSERT((ul5_4.min() == 2) && (ul5_4.max() == 2) && ul5_4.range());

			Interval<int> ul5_5(1,10);
			ul5_5.nq(4);
			ul5_5.nq(5);
			ul5_5.eq(5);
			ASSERT(ul5_5.empty());

			TRACE( std::cout << "*** tests: lq"<< std::endl; )
			Interval<int> ul6_0(1,10);
			ASSERT(ul6_0.lq(11) == false);

			Interval<int> ul6_1(1,10);
			ul6_1.lq(0);
			ASSERT(ul6_1.empty());

			Interval<int> ul6_2(1,20);
			ul6_2.nq(4);
			ul6_2.nq(7);
			ul6_2.nq(11);
			TRACE( std::cout << "!= 4 7 11 --> " << ul6_2.to_string() << std:: endl; )
			ASSERT(ul6_2._range_list.size() == 4);
			ul6_2.lq(11);
			TRACE( std::cout << "<= 11 --> " << ul6_2.to_string() << std:: endl; )
			ASSERT(ul6_2._range_list.size() == 3);
			ul6_2.lq(8);
			TRACE( std::cout << "<= 8 --> " << ul6_2.to_string() << std:: endl; )
			ASSERT(ul6_2._range_list.size() == 3);
			ul6_2.lq(1);
			TRACE( std::cout << "<= 1 --> " << ul6_2.to_string() << std:: endl; )
			ASSERT(ul6_2.range() && (ul6_2._range_list.size() == 1));

			TRACE( std::cout << "*** tests: gq"<< std::endl; )
			Interval<int> ul7_0(1,10);
			ASSERT(ul7_0.gq(0) == false);

			Interval<int> ul7_1(1,10);
			ul7_1.gq(11);
			ASSERT(ul7_1.empty());

			Interval<int> ul7_2(1,20);
			ul7_2.nq(4);
			ul7_2.nq(7);
			ul7_2.nq(11);
			TRACE( std::cout << "!= 4 7 11 --> " << ul7_2.to_string() << std:: endl; )
			ASSERT(ul7_2._range_list.size() == 4);
			ul7_2.gq(4);
			TRACE( std::cout << ">= 4 --> " << ul7_2.to_string() << std:: endl; )
			ASSERT(ul7_2._range_list.size() == 3);
			ul7_2.gq(6);
			TRACE( std::cout << ">= 6 --> " << ul7_2.to_string() << std:: endl; )
			ASSERT(ul7_2._range_list.size() == 3);
			ul7_2.gq(20);
			TRACE( std::cout << ">= 20 --> " << ul7_2.to_string() << std:: endl; )
			ASSERT(ul7_2.range() && (ul7_2._range_list.size() == 1));

			TRACE( std::cout << "*** tests: in"<< std::endl; )
			Interval<int> ul8_0(1,10);
			ASSERT(ul8_0.in(0) == false);
			ASSERT(ul8_0.in(11) == false);
			ASSERT(ul8_0.range() && ul8_0.in(1));
			ASSERT(ul8_0.range() && ul8_0.in(9));
			Interval<int> ul8_1(1,20);
			ul8_1.nq(4);
			ul8_1.nq(7);
			ul8_1.nq(11);
			ul8_1.nq(15);
			TRACE( std::cout << "!= 4 7 11 15 --> " << ul8_1.to_string() << std:: endl; )
			// closer min
			ASSERT(ul8_1.in(4) == false);
			ASSERT(ul8_1.in(5));
			ASSERT(ul8_1.in(6));
			// not closer min
			ASSERT(ul8_1.in(15) == false);
			ASSERT(ul8_1.in(16));
			ASSERT(ul8_1.in(14));
			ASSERT(ul8_1.in(12));

			TRACE( std::cout << "*** tests: copy constructor"<< std::endl; )
			Interval<int> copy_ul8_0(ul8_0,4);
			ASSERT(copy_ul8_0.in(4) == false);
			ASSERT(copy_ul8_0.in(15) == false);
			ASSERT(copy_ul8_0.range() && copy_ul8_0.in(5));
			ASSERT(copy_ul8_0.range() && copy_ul8_0.in(14));
			Interval<int> copy_a_ul8_1(ul8_1,4);
			// closer min
			ASSERT(copy_a_ul8_1.in(4) == false);
			ASSERT(copy_a_ul8_1.in(5));
			ASSERT(copy_a_ul8_1.in(6));
			// not closer min
			ASSERT(copy_a_ul8_1.in(15) == false);
			ASSERT(copy_a_ul8_1.in(16));
			ASSERT(copy_a_ul8_1.in(14));
			ASSERT(copy_a_ul8_1.in(12));
			Interval<int> copy_b__ul8_1(ul8_1,4,true);
			// closer min
			ASSERT(copy_b__ul8_1.in(0) == false);
			ASSERT(copy_b__ul8_1.in(-1));
			ASSERT(copy_b__ul8_1.in(-2));
			// not closer min
			ASSERT(copy_b__ul8_1.in(-11) == false);
			ASSERT(copy_b__ul8_1.in(-12));
			ASSERT(copy_b__ul8_1.in(-10));
			ASSERT(copy_b__ul8_1.in(-8));

			TRACE( std::cout << "*** tests: count"<< std::endl; )
			Interval<int> ul9_0(1,10);
			ASSERT(ul9_0.count() == 10);
			ul9_0.nq(4);
			ul9_0.nq(7);
			TRACE( std::cout << "!= 4 7 --> " << ul9_0.to_string() << std:: endl; )
			ASSERT(ul9_0.count() == 8);

			TRACE( std::cout << "*** tests: append_range"<< std::endl; )
			Interval<int> ul10_0(1,10);
			ASSERT(ul10_0.count() == 10);
			ul10_0.append_range(10,11);
			TRACE( std::cout << "append [10,11] --> " << ul10_0.to_string() << std:: endl; )
			ASSERT(ul10_0.count() == 11);
			ul10_0.append_range(13,14);
			TRACE( std::cout << "append [13,14] --> " << ul10_0.to_string() << std:: endl; )
			ASSERT(ul10_0.count() == 13);
			Interval<int> ul10_1;
			ASSERT(ul10_1.empty());
			ul10_1.append_range(10,11);
			TRACE( std::cout << "append [10,11] --> " << ul10_1.to_string() << std:: endl; )
			ASSERT(ul10_1.count() == 2);

			{
				TRACE( std::cout << "*** tests on short int"<< std::endl; )
				TRACE( std::cout << "*** tests: gq"<< std::endl; )
				Interval<short int> ul7_0(1,10);
				ASSERT(ul7_0.gq(0) == false);

				Interval<short int> ul7_1(1,10);
				ul7_1.gq(11);
				ASSERT(ul7_1.empty());

				Interval<short int> ul7_2(1,20);
				ul7_2.nq(4);
				ul7_2.nq(7);
				ul7_2.nq(11);
				TRACE( std::cout << "!= 4 7 11 --> " << ul7_2.to_string() << std:: endl; )
					ASSERT(ul7_2._range_list.size() == 4);
				ul7_2.gq(4);
				TRACE( std::cout << ">= 4 --> " << ul7_2.to_string() << std:: endl; )
					ASSERT(ul7_2._range_list.size() == 3);
				ul7_2.gq(6);
				TRACE( std::cout << ">= 6 --> " << ul7_2.to_string() << std:: endl; )
					ASSERT(ul7_2._range_list.size() == 3);
				ul7_2.gq(20);
				TRACE( std::cout << ">= 20 --> " << ul7_2.to_string() << std:: endl; )
					ASSERT(ul7_2.range() && (ul7_2._range_list.size() == 1));

				TRACE( std::cout << "*** tests: in"<< std::endl; )
					Interval<short int> ul8_0(1,10);
				ASSERT(ul8_0.in(0) == false);
				ASSERT(ul8_0.in(11) == false);
				ASSERT(ul8_0.range() && ul8_0.in(1));
				ASSERT(ul8_0.range() && ul8_0.in(9));
				Interval<short int> ul8_1(1,20);
				ul8_1.nq(4);
				ul8_1.nq(7);
				ul8_1.nq(11);
				ul8_1.nq(15);
				TRACE( std::cout << "!= 4 7 11 15 --> " << ul8_1.to_string() << std:: endl; )
				// closer min
				ASSERT(ul8_1.in(4) == false);
				ASSERT(ul8_1.in(5));
				ASSERT(ul8_1.in(6));
				// not closer min
				ASSERT(ul8_1.in(15) == false);
				ASSERT(ul8_1.in(16));
				ASSERT(ul8_1.in(14));
				ASSERT(ul8_1.in(12));
			}

			{
				TRACE( std::cout << "*** tests on long int"<< std::endl; )
				TRACE( std::cout << "*** tests: gq"<< std::endl; )
				Interval<long int> ul7_0(1,10);
				ASSERT(ul7_0.gq(0) == false);

				Interval<long int> ul7_1(1,10);
				ul7_1.gq(11);
				ASSERT(ul7_1.empty());

				Interval<long int> ul7_2(1,20);
				ul7_2.nq(4);
				ul7_2.nq(7);
				ul7_2.nq(11);
				TRACE( std::cout << "!= 4 7 11 --> " << ul7_2.to_string() << std:: endl; )
				ASSERT(ul7_2._range_list.size() == 4);
				ul7_2.gq(4);
				TRACE( std::cout << ">= 4 --> " << ul7_2.to_string() << std:: endl; )
				ASSERT(ul7_2._range_list.size() == 3);
				ul7_2.gq(6);
				TRACE( std::cout << ">= 6 --> " << ul7_2.to_string() << std:: endl; )
				ASSERT(ul7_2._range_list.size() == 3);
				ul7_2.gq(20);
				TRACE( std::cout << ">= 20 --> " << ul7_2.to_string() << std:: endl; )
				ASSERT(ul7_2.range() && (ul7_2._range_list.size() == 1));

				TRACE( std::cout << "*** tests: in"<< std::endl; )
				Interval<long int> ul8_0(1,10);
				ASSERT(ul8_0.in(0) == false);
				ASSERT(ul8_0.in(11) == false);
				ASSERT(ul8_0.range() && ul8_0.in(1));
				ASSERT(ul8_0.range() && ul8_0.in(9));
				Interval<long int> ul8_1(1,20);
				ul8_1.nq(4);
				ul8_1.nq(7);
				ul8_1.nq(11);
				ul8_1.nq(15);
				TRACE( std::cout << "!= 4 7 11 15 --> " << ul8_1.to_string() << std:: endl; )
				// closer min
				ASSERT(ul8_1.in(4) == false);
				ASSERT(ul8_1.in(5));
				ASSERT(ul8_1.in(6));
				// not closer min
				ASSERT(ul8_1.in(15) == false);
				ASSERT(ul8_1.in(16));
				ASSERT(ul8_1.in(14));
				ASSERT(ul8_1.in(12));
			}
		}

		/**
		 * Test on chr::Interval::narrow function
		 */
		bool vector_and_interval_equal(const std::vector<int>& v, const chr::Interval<int,false>& iv)
		{
			unsigned int nb = 0;
			auto it = iv.begin();
			auto it_end = iv.end();

			for ( ; it != it_end; ++it)
			{
				++nb;
				if (std::find(v.begin(), v.end(), (*it)) == v.end()) {
					return false;
				}
			}
			return v.size() == nb;
		}

		std::string to_string(const std::vector<int>& v)
		{
			std::string str = "{";
			bool first = true;
			for (auto x : v)
				if (first) {
					str += std::to_string(x);
					first = false;
				} else
					str += "," + std::to_string(x);

			str += "}";
			return str;
		}

		void narrow_tests()
		{
			using namespace chr;

			TRACE( std::cout << "*** tests: narrow() function"<< std::endl; )
			std::vector< std::vector< std::vector<int> > > idxs_to_remove {
				{ {5,18,1,13,8,4,7,8,4,8,8,1,5,4,2}, {14,16,2,15,13,0,0,3,3,4,3,7,3,6,2,1,0,0,0,0} },
				{ {18,2,12,1,7,5,7}, {15,7,14,5,5,1,6,8,2,0,3,6,1,3,2,3,0,1,1,0} },
				{ {}, {5,1,0,14,12,10,12,11,9,2,2,4,2,5,5,1,1,0,1,0} },
				{ {}, {15,11,5,12,1,8,5,8,3,0,1,1,7,4,0,0,0,0} },
				{ {6,3,8,10,6,14,7,10,10,2,6,8,4,1,4,2,0,0,1}, {0} },
				{ {1,14,9,14,15,7,12,5,3,5,6,8,5,3,2,3,1,1,1}, {14,14,6,5,5,13,9,12,7,10,3,5,3,6,5,4,1,1,1} },
				{ {2}, {9,11,5} },
				{ {8,16,10,6,4,11}, {17,10} },
				{ {16,17,12,5,5,10,1,6,5,10,5,7,5,5,5,1,1}, {4,8} },
				{ {16,14,11,10,10}, {12,7,15,14,11,3,6,10,11} },
				{ {19,14,5,7,7,1}, {0,12,4,8,3,11,6,6,4,10,9} },
				{ {10,14,2,8,14,3,12,5,5,2,8,7,5,6,4,3,0,0,0}, {1,13,11,5,6,0,3,7,11,0,3,7,4,3,5} },
				{ {4,9,0,11,1,14,5,4,1,7,4,8,0,5,0}, {17,12,0,8,6,8,1,7,11,10,9,2,7,3,4,2,1} },
				{ {4,17,9,6,1,8,4,11,8,9,4,7,1,0}, {1,3,8,15,1,5,4,4,5,8,3,7,3,2,4,1,3,0} },
				{ {1,1,10,3,8,0,3,10,10,9,1,7,3,5}, {2,12,11,13,1,11,4,6,11,1,7,7,4,4,5,2,0,0,1} }
			};

			for (unsigned int k = 0; k < idxs_to_remove.size(); ++k)
			{
				std::vector<int> s1{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
				std::vector<int> s2{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
				std::vector<int> s3;
				chr::Interval<int,false > i1(1,20);
				chr::Interval<int,false > i2(1,20);

				auto& cur_test = idxs_to_remove[k];
				for (unsigned int i = 0; i < cur_test[0].size(); ++i)
				{
					auto idx = cur_test[0][i];
					i1.nq(s1[idx]);
					s1.erase(s1.begin()+idx);
				}
				for (unsigned int i = 0; i < cur_test[1].size(); ++i)
				{
					auto idx = cur_test[1][i];
					i2.nq(s2[idx]);
					s2.erase(s2.begin()+idx);
				}

				std::set_intersection(	s1.begin(), s1.end(),
						s2.begin(), s2.end(),
						std::back_inserter(s3));
				i1.narrow(i2);

				TRACE( std::cout << "--> s1:" << to_string(s1) << " == i1:" << i1.to_string() << std::endl; )
				TRACE( std::cout << "--> s2:" << to_string(s2) << " == i2:" << i2.to_string() << std::endl; )
				TRACE( std::cout << "## s3:" << to_string(s3) << " == i1:" << i1.to_string() << std::endl; )
				ASSERT(vector_and_interval_equal(s3,i1));
			}
		}

		void minus_tests()
		{
			using namespace chr;

			TRACE( std::cout << "*** tests: minus() function"<< std::endl; )
			std::vector< std::vector< std::vector<int> > > idxs_to_remove {
				{ {12,0}, {5,1,10,12,14,5,3,3,11,2,9,7,3,0,4,2,0,2,0,0} }, 
				{ {2,0,13,14,0,12,8,7,1,5,6}, {15,6,1,16,11,9,4,1,3,5,9,3,5,4,5,3,0,1,1,0} }, 
				{ {}, {3,1,9,8,15,2,0,9,5,3,9,3,4,5,1,0,2,0,0} }, 
				{ {6,5,4,12,9,11,9,2,6,5,2,0,6,2,2,2,0,2,0}, {19} }, 
				{ {14,3,14,13,14,5,1,2,10,2,4,2,6,4,0,0,1,1,1}, {19,5,13,4,10,2,8,1,8,1,9,8,6,5,5,0} }, 
				{ {3,4,13,5,10,5,4,2,10,9,6,8,2,3,2,2,2,2}, {2,13,8,6,3,4,10,8,3,8,7,0,7,1,5,1,3,2,1} }, 
				{ {}, {7,3,14,7,9,5,7,2,1,8,7,4,0,5,0,3,1,0,0} }, 
				{ {}, {0} }, 
				{ {}, {19} }, 
				{ {18,12,15,2,13,6,3,8,3,2,8,3,5,3,4,3,2,2}, {7,13,3,8,6,4,1,8,5,10,6,7,4,4,5,2,2,2,1} }, 
				{ {11,13,16,1,10,5,12,8,2,8,4,8,7,3}, {8,11,1,11,12,13,9,11,0,9,8,0,4,1,2,4,1,0} }, 
				{ {4,6,0,16,2,2,9,4,7,3,7,0,0,1,2,4,2,1}, {15,15} }, 
				{ {17,2,4}, {13,5,15,10,13,7,4} }, 
				{ {7,4,16,1,0,3,8,8,4,4,7,3,7,1,2}, {6,9,3,5,9,13,0,7,2,7,9,7,4,1,0,3,3} }, 
				{ {3,3,17,9,6,3,4,1,10,0,3,0}, {9,4,7,2} }, 
				{ {10,14,4,10,4}, {7,11,8,5,15,11,7,5,6} }, 
				{ {13,16,8,8,8,13,8,12,3,3,4,8,3,1,2}, {12,7,8,5,1,10} }, 
				{ {17,4,15,6,9,5,8,0,0,8,0,2,5,2,0,2}, {17,15,2,11} }, 
				{ {18,16,2,14,10,12,12,11,4,5,2,1,4,4,4,3,1}, {16,8,15,3,13,1,6,2,5,8,4,7} }, 
				{ {15,8,8,0,7,7,11}, {17} }, 
			};

			for (unsigned int k = 0; k < idxs_to_remove.size(); ++k)
			{
				std::vector<int> s1{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
				std::vector<int> s2{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
				std::vector<int> s3;
				chr::Interval<int,false > i1(1,20);
				chr::Interval<int,false > i2(1,20);

				auto& cur_test = idxs_to_remove[k];
				for (unsigned int i = 0; i < cur_test[0].size(); ++i)
				{
					auto idx = cur_test[0][i];
					i1.nq(s1[idx]);
					s1.erase(s1.begin()+idx);
				}
				for (unsigned int i = 0; i < cur_test[1].size(); ++i)
				{
					auto idx = cur_test[1][i];
					i2.nq(s2[idx]);
					s2.erase(s2.begin()+idx);
				}

				std::set_difference(s1.begin(), s1.end(),
						s2.begin(), s2.end(),
						std::back_inserter(s3));
				i1.minus(i2);

				TRACE( std::cout << "--> s1:" << to_string(s1) << " == i1:" << i1.to_string() << std::endl; )
				TRACE( std::cout << "--> s2:" << to_string(s2) << " == i2:" << i2.to_string() << std::endl; )
				TRACE( std::cout << "## s3:" << to_string(s3) << " == i1:" << i1.to_string() << std::endl; )
				ASSERT(vector_and_interval_equal(s3,i1));
			}
		}

		/**
		 * Test on chr::Interval basics functions
		 */
		void iterator_tests()
		{
			using namespace chr;
			TRACE( std::cout << "*** tests: iterators"<< std::endl; )
			Interval<int> ul0(1,8);
			std::set<int> s_ul0({1, 2, 3, 4, 5, 6, 7, 8});
			ASSERT((ul0.min() == 1) && (ul0.max() == 8) && ul0.range());
			ASSERT(ul0.width() == s_ul0.size());

			auto it0 = ul0.begin();
			auto it0_end = ul0.end();
			auto s_it0 = s_ul0.begin();
			auto s_it0_end = s_ul0.end();
			
			for ( ; it0 != it0_end; ++it0)
			{
				ASSERT(*it0 == *s_it0);
				++s_it0;
			}
			ASSERT(s_it0 == s_it0_end);

			s_ul0.erase(3); ul0.nq(3);
			s_ul0.erase(7); ul0.nq(7);
			s_ul0.erase(8); ul0.nq(8);

			auto it1 = ul0.begin();
			auto it1_end = ul0.end();
			auto s_it1 = s_ul0.begin();
			auto s_it1_end = s_ul0.end();
			
			for ( ; it1 != it1_end; ++it1)
			{
				std::cout << *it1 << " " << *s_it1 << std::endl;
				ASSERT(*it1 == *s_it1);
				++s_it1;
			}
			ASSERT(s_it1 == s_it1_end);

			auto it_range1 = ul0.range_begin();
			auto it_range1_end = ul0.range_end();
			unsigned int cpt_range = 0;
			for ( ; it_range1 != it_range1_end; ++it_range1)
			{
				if (cpt_range == 0) ASSERT(((*it_range1)._min == 1) && ((*it_range1)._max == 2));
				if (cpt_range == 1) ASSERT(((*it_range1)._min == 4) && ((*it_range1)._max == 6));
				++cpt_range;
			}
			ASSERT(cpt_range == 2);


			Interval<int,true> ul1(1,8);
			ASSERT((ul1.min() == 1) && (ul1.max() == 8) && ul1.range());

			auto it2 = ul1.begin();
			auto it2_end = ul1.end();
			int i=1;
			
			for ( ; it2 != it2_end; ++it2)
			{
				ASSERT(*it2 == i);
				++i;
			}

			TRACE( std::cout << "*** tests: nq() range iterator"<< std::endl; )
			{
				Interval<int> ul1_0(1,20);
				Interval<int> ul1_1(1,20);
				auto itr = ul1_1.range_begin();
				ul1_0.nq(4);
				ul1_1.nq(itr,4);
				ul1_0.nq(7);
				ul1_1.nq(itr,7);
				ul1_0.nq(11);
				ul1_1.nq(itr,11);
				ul1_0.nq(15);
				ul1_1.nq(itr,15);
				ul1_0.nq(20);
				ul1_1.nq(itr,20);
				TRACE( std::cout << "!= 4 7 11 15 20 --> " << ul1_0.to_string() << std:: endl; )
				ASSERT(ul1_0 == ul1_1);
			}
		}

		/**
		 * Test on chr::Interval basics functions
		 */
		void basic_tests_float()
		{
			using namespace chr;
			TRACE( std::cout << "*** tests: nq"<< std::endl; )
			Interval<float> ul0(1,1);
			ASSERT((ul0.min() == 1) && (ul0.max() == 1) && ul0.range());
			ul0.nq(1);
			ASSERT(ul0.empty());

			Interval<float> ul1(1,6);
			TRACE( std::cout << ul1.to_string() << std:: endl; )
			ASSERT((ul1.min() == 1) && (ul1.max() == 6) && ul1.range());
			ul1.nq(1);
			TRACE( std::cout << "!= 1 --> " << ul1.to_string() << std:: endl; )
			ASSERT((ul1.min() == chr::Numerics<float>::next(1)) && (ul1.max() == 6) && ul1.range());
			ul1.nq(6);
			TRACE( std::cout << "!= 6 --> " << ul1.to_string() << std:: endl; )
			ASSERT((ul1.min() == chr::Numerics<float>::next(1)) && (ul1.max() == chr::Numerics<float>::previous(6)) && ul1.range());

			// Tests not range
			ul1.nq(4);
			ASSERT((ul1.min() == chr::Numerics<float>::next(1)) && (ul1.max() == chr::Numerics<float>::previous(6)) && !ul1.range());
			TRACE( std::cout << "!= 4 --> " << ul1.to_string() << std:: endl; )
			ul1.nq(3);
			TRACE( std::cout << "!= 3 --> " << ul1.to_string() << std:: endl; )
			ul1.nq(5);
			TRACE( std::cout << "!= 5 --> " << ul1.to_string() << std:: endl; )

			Interval<float> ul2(1,10);
			std::cout << ul2.to_string() << std:: endl;
			for (int i=2; i <= 8; i+=2)
			{
				ul2.nq(i);
			}
			std::string str2 = ul2._range_list.to_string(1); 
			TRACE( std::cout << ul2.to_string() << std:: endl; )
			ASSERT((ul2.min() == 1) && (ul2.max() == 10) && !ul2.range());
			ul2.nq(2);
			ASSERT(str2 == ul2._range_list.to_string(1)); 

			// Tests not range (closer min)
			Interval<float> ul3(1,20);
			ul3.nq(3);
			ul3.nq(7);
			ul3.nq(9);
			TRACE( std::cout << "nq (closer_min) --> " << ul3.to_string() << std:: endl; )
			ASSERT((ul3.min() == 1) && (ul3.max() == 20) && !ul3.range());
			ul3.nq(8);
			TRACE( std::cout << "!= 8 --> " << ul3.to_string() << std:: endl; )
			ul3.nq(4);
			TRACE( std::cout << "!= 4 --> " << ul3.to_string() << std:: endl; )
			ul3.nq(6);
			TRACE( std::cout << "!= 6 --> " << ul3.to_string() << std:: endl; )

			// Tests not range (not closer min)
			Interval<float> ul4(1,20);
			ul4.nq(17);
			ul4.nq(13);
			ul4.nq(11);
			TRACE( std::cout << "nq (not closer_min) --> " << ul4.to_string() << std:: endl; )
			ASSERT((ul4.min() == 1) && (ul4.max() == 20) && !ul4.range());
			ul4.nq(12);
			TRACE( std::cout << "!= 12 --> " << ul4.to_string() << std:: endl; )
			ul4.nq(16);
			TRACE( std::cout << "!= 16 --> " << ul4.to_string() << std:: endl; )
			ul4.nq(14);
			TRACE( std::cout << "!= 14 --> " << ul4.to_string() << std:: endl; )

			TRACE( std::cout << "*** tests: eq"<< std::endl; )
			Interval<float> ul5_0(1,1);
			ASSERT(ul5_0.eq(1) == false);

			Interval<float> ul5_1(1,1);
			ul5_1.eq(2);
			TRACE( std::cout << "== 2 --> " << ul5_1.to_string() << std:: endl; )
			ASSERT(ul5_1.empty());

			Interval<float> ul5_2(1,1);
			ul5_2.eq(0);
			TRACE( std::cout << "== 0 --> " << ul5_2.to_string() << std:: endl; )
			ASSERT(ul5_2.empty());

			Interval<float> ul5_3(1,4);
			ul5_3.eq(2);
			TRACE( std::cout << "== 2 --> " << ul5_3.to_string() << std:: endl; )
			ASSERT((ul5_3.min() == 2) && (ul5_3.max() == 2) && ul5_3.range());

			TRACE( std::cout << "*** tests: eq (not range)"<< std::endl; )
			Interval<float> ul5_4(1,10);
			ul5_4.nq(4);
			ul5_4.eq(2);
			TRACE( std::cout << "== 2 --> " << ul5_4.to_string() << std:: endl; )
			ASSERT((ul5_4.min() == 2) && (ul5_4.max() == 2) && ul5_4.range());

			Interval<float> ul5_5(1,10);
			ul5_5.nq(4);
			ul5_5.nq(5);
			ul5_5.eq(5);
			ASSERT(ul5_5.empty());

			TRACE( std::cout << "*** tests: lq"<< std::endl; )
			Interval<float> ul6_0(1,10);
			ASSERT(ul6_0.lq(11) == false);

			Interval<float> ul6_1(1,10);
			ul6_1.lq(0);
			ASSERT(ul6_1.empty());

			Interval<float> ul6_2(1,20);
			ul6_2.nq(4);
			ul6_2.nq(7);
			ul6_2.nq(11);
			TRACE( std::cout << "!= 4 7 11 --> " << ul6_2.to_string() << std:: endl; )
			ASSERT(ul6_2._range_list.size() == 4);
			ul6_2.lq(11);
			TRACE( std::cout << "<= 11 --> " << ul6_2.to_string() << std:: endl; )
			ASSERT(ul6_2._range_list.size() == 3);
			ul6_2.lq(8);
			TRACE( std::cout << "<= 8 --> " << ul6_2.to_string() << std:: endl; )
			ASSERT(ul6_2._range_list.size() == 3);
			ul6_2.lq(1);
			TRACE( std::cout << "<= 1 --> " << ul6_2.to_string() << std:: endl; )
			ASSERT(ul6_2.range() && (ul6_2._range_list.size() == 1));

			TRACE( std::cout << "*** tests: gq"<< std::endl; )
			Interval<float> ul7_0(1,10);
			ASSERT(ul7_0.gq(0) == false);

			Interval<float> ul7_1(1,10);
			ul7_1.gq(11);
			ASSERT(ul7_1.empty());

			Interval<float> ul7_2(1,20);
			ul7_2.nq(4);
			ul7_2.nq(7);
			ul7_2.nq(11);
			TRACE( std::cout << "!= 4 7 11 --> " << ul7_2.to_string() << std:: endl; )
			ASSERT(ul7_2._range_list.size() == 4);
			ul7_2.gq(4);
			TRACE( std::cout << ">= 4 --> " << ul7_2.to_string() << std:: endl; )
			ASSERT(ul7_2._range_list.size() == 3);
			ul7_2.gq(6);
			TRACE( std::cout << ">= 6 --> " << ul7_2.to_string() << std:: endl; )
			ASSERT(ul7_2._range_list.size() == 3);
			ul7_2.gq(20);
			TRACE( std::cout << ">= 20 --> " << ul7_2.to_string() << std:: endl; )
			ASSERT(ul7_2.range() && (ul7_2._range_list.size() == 1));

			TRACE( std::cout << "*** tests: in"<< std::endl; )
			Interval<float> ul8_0(1,10);
			ASSERT(ul8_0.in(0) == false);
			ASSERT(ul8_0.in(11) == false);
			ASSERT(ul8_0.range() && ul8_0.in(1));
			ASSERT(ul8_0.range() && ul8_0.in(9));
			Interval<float> ul8_1(1,20);
			ul8_1.nq(4);
			ul8_1.nq(7);
			ul8_1.nq(11);
			ul8_1.nq(15);
			TRACE( std::cout << "!= 4 7 11 15 --> " << ul8_1.to_string() << std:: endl; )
			// closer min
			ASSERT(ul8_1.in(4) == false);
			ASSERT(ul8_1.in(5.2));
			ASSERT(ul8_1.in(6));
			// not closer min
			ASSERT(ul8_1.in(15) == false);
			ASSERT(ul8_1.in(16.2));
			ASSERT(ul8_1.in(14));
			ASSERT(ul8_1.in(12.5));

			TRACE( std::cout << "*** tests: count"<< std::endl; )
			Interval<float> ul9_0(1,10);
			ASSERT(ul9_0.count() == std::numeric_limits<size_t>::infinity());
			ul9_0.nq(4);
			ul9_0.nq(7);
			TRACE( std::cout << "!= 4 7 --> " << ul9_0.to_string() << std:: endl; )
			ASSERT(ul9_0.count() == std::numeric_limits<size_t>::infinity());

			{
				TRACE( std::cout << "*** tests: in (double)"<< std::endl; )
				Interval<double> ul8_0(1,10);
				ASSERT(ul8_0.in(0) == false);
				ASSERT(ul8_0.in(11) == false);
				ASSERT(ul8_0.range() && ul8_0.in(1));
				ASSERT(ul8_0.range() && ul8_0.in(9));
				Interval<double> ul8_1(1,20);
				ul8_1.nq(4);
				ul8_1.nq(7);
				ul8_1.nq(11);
				ul8_1.nq(15);
				TRACE( std::cout << "!= 4 7 11 15 --> " << ul8_1.to_string() << std:: endl; )
				// closer min
				ASSERT(ul8_1.in(4) == false);
				ASSERT(ul8_1.in(5.2));
				ASSERT(ul8_1.in(6));
				// not closer min
				ASSERT(ul8_1.in(15) == false);
				ASSERT(ul8_1.in(16.2));
				ASSERT(ul8_1.in(14));
				ASSERT(ul8_1.in(12.5));
			}
		}

		

		/**
		 * Backtrack tests on interval
		 */
		struct Interval_wrapper : public chr::Backtrack_observer
		{
			chr::Interval< int > _iv;
			Interval_wrapper(int min, int max) : _iv(min,max) { }
			virtual ~Interval_wrapper() {}
			bool rewind(chr::Depth_t previous_depth, chr::Depth_t new_depth) override
			{
				return _iv.rewind(previous_depth,new_depth);
			}
		};

		void backtrack_tests()
		{
			using namespace chr;
			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			std::string str1, str2, str3;
			auto shared_iv1 = chr::make_shared< Interval_wrapper >(1,20);
			auto* iv1 = &shared_iv1->_iv;
			Backtrack::schedule(shared_iv1._ptr);

			TRACE( std::cout << "* 1 *******************" << std::endl; )
			Depth_t d = Backtrack::depth();
			TRACE( std::cout << "Initial: " << iv1->to_string() << std::endl; )
			str1 = iv1->to_string();

			Backtrack::inc_backtrack_depth(); //backtrack_depth = 1
			iv1->nq(4);
			iv1->nq(7);
			iv1->nq(11);
			iv1->nq(15);
			TRACE( std::cout << "!= 4 7 11 15 --> " << iv1->to_string() << std:: endl; )
			ASSERT(iv1->_range_list.size() == 5);
			str2 = iv1->to_string();

			Backtrack::inc_backtrack_depth(); //backtrack_depth = 2
			iv1->gq(11);
			ASSERT(iv1->_range_list.size() == 2);
			str3 = iv1->to_string();

			Backtrack::inc_backtrack_depth(); //backtrack_depth = 3
			iv1->lq(12);
			ASSERT(iv1->_range_list.size() == 1);
			ASSERT(iv1->singleton());

			Backtrack::back_to(d+2);
			TRACE( std::cout << "Back to previous: " << iv1->to_string() << std::endl; )
			ASSERT(str3 == iv1->to_string());

			Backtrack::back_to(d+1);
			TRACE( std::cout << "Back to previous: " << iv1->to_string() << std::endl; )
			ASSERT(str2 == iv1->to_string());

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to first: " << iv1->to_string() << std::endl; )
			ASSERT(str1 == iv1->to_string());

			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			auto shared_iv2 = chr::make_shared< Interval_wrapper >(0,4);
			auto* iv2 = &shared_iv2->_iv;
			Backtrack::schedule(shared_iv2._ptr);

			TRACE( std::cout << "* 2 *******************" << std::endl; )
			d = Backtrack::depth();
			iv2->nq(2);
			TRACE( std::cout << "Initial: " << iv2->to_string() << std::endl; )
			str1 = iv2->to_string();
			Backtrack::inc_backtrack_depth(); //backtrack_depth = 1
			iv2->nq(1);
			iv2->eq(0);
			Backtrack::back_to(d);
			TRACE( std::cout << "Back to first: " << iv2->to_string() << std::endl; )
			ASSERT(str1 == iv2->to_string());

			TRACE( std::cout << "* 3 *******************" << std::endl; )
			ASSERT(Backtrack::depth() == 0);
			auto shared_iv3 = chr::make_shared< Interval_wrapper >(1,20);
			auto* iv3 = &shared_iv3->_iv;
			Backtrack::schedule(shared_iv3._ptr);

			d = Backtrack::depth();
			ASSERT(d == 0);

			iv3->nq(10);
			TRACE( std::cout << "Initial state:" << iv3->to_string() << std::endl; )
			str1 = iv3->_range_list.to_string(1);

			Backtrack::inc_backtrack_depth(); // Backtrack depth = 1
			iv3->nq(9);
			iv3->nq(11);
			iv3->eq(1);
			TRACE( std::cout << "Inc depth and replace:" << iv3->to_string() << std::endl; )

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to first:" << iv3->to_string() << std::endl; )
			ASSERT(str1 == iv3->_range_list.to_string(1));

			TRACE( std::cout << "* 4 *******************" << std::endl; )
			ASSERT(Backtrack::depth() == 0);
			Logical_var_mutable< chr::Interval<int, false> > X( Interval<int, false>(1,4) );
			Logical_var_mutable< chr::Interval<int, false> > Y( Interval<int, false>(7,9) );

			d = Backtrack::depth();
			ASSERT(d == 0);

			TRACE( std::cout << "Initial state:" << X.to_string() << " " << Y.to_string() << std::endl; )
			str1 = X.to_string() + Y.to_string();

			Backtrack::inc_backtrack_depth(); // Backtrack depth = 1
			X %= Y;
			TRACE( std::cout << "Unify:" << X.to_string() << " " << Y.to_string() << std::endl; )

			Backtrack::back_to(d);
			TRACE( std::cout << "Back to first:" << X.to_string() << " " << Y.to_string() << std::endl; )
			ASSERT(str1 == X.to_string() + Y.to_string());
		}

	}

	void run_bt_interval_tests(cute::suite& s)
	{
		s.push_back( { "Interval, basic tests on range intervals", bt_interval::basic_tests_range } );
		s.push_back( { "Interval, basic tests", bt_interval::basic_tests } );
		s.push_back( { "Interval, narrow tests", bt_interval::narrow_tests } );
		s.push_back( { "Interval, minus tests", bt_interval::minus_tests } );
		s.push_back( { "Interval, iterator tests", bt_interval::iterator_tests } );
		s.push_back( { "Interval, basic tests on float", bt_interval::basic_tests_float } );
		s.push_back( { "Interval, backtrack tests", bt_interval::backtrack_tests } );
	}
}
