/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#ifndef RUNTIME_TESTS_HH_
#define RUNTIME_TESTS_HH_

#include <cute.h>
#include <cute_runner.h>

namespace tests {
	/**
	 * Run the tests on chr::Interval
	 * @param s The test suite to use to post tests
	 */
	void run_bt_interval_tests(cute::suite& s);

	/**
	 * Run the tests on the bt_list
	 * @param s The test suite to use to post tests
	 */
	void run_bt_list_tests(cute::suite& s);

	/**
	 * Run the tests on Logical_var
	 * @param s The test suite to use to post tests
	 */
	void run_logical_var_tests(cute::suite& s);

	/**
	 * Run the tests on Constraint_store
	 * @param s The test suite to use to post tests
	 */
	void run_constraint_store_tests(cute::suite& s);

	/**
	 * Run the tests on History
	 * @param s The test suite to use to post tests
	 */
	void run_history_tests(cute::suite& s);

	/**
	 * Run more advanced tests on Backtrack (manager and backtrackable data structures)
	 * @param s The test suite to use to post tests
	 */
	void run_advanced_backtrack_tests(cute::suite& s);
}

#endif /* RUNTIME_TESTS_HH_ */
