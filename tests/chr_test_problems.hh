/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#ifndef TEST_PROBLEMS_HH_
#define TEST_PROBLEMS_HH_

#include <cute.h>
#include <cute_runner.h>

namespace tests {
	/**
	 * Run the local variable declaration test problems
	 * @param s The test suite to use to post tests
	 */
	void run_local_variable_declaration_problem(cute::suite& s);

	/**
	 * Run the index generation test problems
	 * @param s The test suite to use to post tests
	 */
	void run_index_generation_problem(cute::suite& s);

	/**
	 * Run the constraint wake up test problems
	 * @param s The test suite to use to post tests
	 */
	void run_constraint_wake_up_tests(cute::suite& s);

	/**
	 * Run the mutable variables test problems
	 * @param s The test suite to use to post tests
	 */
	void run_mutable_variable_tests(cute::suite& s);

	/**
	 * Run the advanced test problems about history
	 * @param s The test suite to use to post tests
	 */
	void run_advanced_history_tests(cute::suite& s);

	/**
	 * Run the test problems about propagation rules
	 * with no history
	 * @param s The test suite to use to post tests
	 */
	void run_propagation_noh_problem(cute::suite& s);

	/**
	 * Run the never stored test problem
	 * @param s The test suite to use to post tests
	 */
	void run_never_stored_problem(cute::suite& s);

	/**
	 * Run the advanced test problems about r_list
	 * (own user data structure design)
	 * @param s The test suite to use to post tests
	 */
	void run_r_list_problems(cute::suite& s);

	/**
	 * Run the advanced test problems about chr_count
	 * which count the number of constraints in a store
	 * of constraints.
	 * @param s The test suite to use to post tests
	 */
	void run_chr_count_problems(cute::suite& s);

	/**
	 * Run the advanced test problems about logical
     * data structure (Logical_vector, Logical_list ...)
	 * @param s The test suite to use to post tests
	 */
	void run_logical_structure_problems(cute::suite& s);

	/**
	 * Run the bang constraint test problem
	 * @param s The test suite to use to post tests
	 */
	void run_bang_problem(cute::suite& s);

	/**
	 * Run the test problems about chr_include directive
	 * @param s The test suite to use to post tests
	 */
	void run_chr_include_problems(cute::suite& s);

	/**
	 * Run the GCD test problems
	 * @param s The test suite to use to post tests
	 */
	void run_sum_problems(cute::suite& s);

	/**
	 * Run the GCD test problems
	 * @param s The test suite to use to post tests
	 */
	void run_gcd_problems(cute::suite& s);

	/**
	 * Run the Fib test problems
	 * @param s The test suite to use to post tests
	 */
	void run_fib_problems(cute::suite& s);

	/**
	 * Run the N-Queens test problem
	 * @param s The test suite to use to post tests
	 */
	void run_nqueens_problems(cute::suite& s);

	/**
	 * Run the Sudoku test problem
	 * @param s The test suite to use to post tests
	 */
	void run_sudoku_problems(cute::suite& s);

	/**
	 * Run the NimFibo test problem
	 * @param s The test suite to use to post tests
	 */
	void run_nimfibo_problems(cute::suite& s);
}

#endif /* TEST_PROBLEMS_HH_ */
