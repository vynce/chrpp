----------
From line 1 to 5
----------
b<b(  )> { a<a(  )> } -->
a<a(  )> { b<b(  )> } -->
----------
(constraint) ##a(  ), persistent
(constraint) b(  ), persistent
(rule) a(  ) <=> failure ;;
(rule) test @ a(  ) \ b(  ) <=> success ;;
(occ rule) [-a(  )#0][] --> failure ;;
----------
----------
