----------
From line 1 to 5
----------
res<res( N )> { gcd<gcd( N )> } -->
gcd<gcd( N )> { res<res( N )>, gcd<gcd( M )> } --> gcd<gcd( ( M - N ) )>
----------
(constraint) gcd( ( +unsigned int ) ), indexes: { <0> }, persistent
(constraint) res( ( -unsigned int ) ), indexes: { <0> }, persistent
(rule) gcd1 @ gcd( 0u ) <=> true ;;
(rule) gcd2 @ gcd( N ) \ gcd( M ) <=> ( N <= M ) | gcd( ( M - N ) ) ;;
(rule) res @ gcd( N ) \ res( N ) <=> true ;;
(occ rule) gcd1 @ [-gcd( 0u )#0][] --> true ;;
(occ rule) gcd2 @ [-gcd( M )#1][+gcd( N ), ( N <= M )] --> gcd( ( M - N ) ) ;;
(occ rule) gcd2 @ [+gcd( N )#2][-gcd( M ), ( N <= M )] --> gcd( ( M - N ) ) ;;
(occ rule) res @ [+gcd( N )#3][-res( N )<idx#0>] --> true ;;
(occ rule) res @ [-res( N )#0][+gcd( N )<idx#0>] --> true ;;
----------
----------
