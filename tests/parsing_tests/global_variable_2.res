----------
From line 1 to 5
----------
test<test( A, B )> { test<test( A, C )> } --> built-in<B>
----------
(constraint) test( ( ?int ), ( ?int ) ), indexes: { <0> }, persistent
(rule) test( A, B ), test( A, C ) ==> ( A == D ) | D %= B ;;
(occ rule) [+test( A, B )#0][( A == D ), +test( A, C )<idx#0>] --> D %= B ;;
(occ rule) [+test( A, C )#1][( A == D ), +test( A, B )<idx#0>] --> D %= B ;;
----------
----------
