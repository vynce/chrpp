----------
From line 1 to 4
----------
test<test( A, B )> { test<test( A, C )> } --> test<test( C, D )>, built-in<A>
----------
(constraint) test( ( ?int ), ( ?int ) ), indexes: { <0> }, persistent
(rule) test( A, B ), test( A, C ) ==> ( D %= A, E %= D, test( C, D ) ) ;;
(occ rule) [+test( A, B )#0][+test( A, C )<idx#0>] --> ( D %= A, E %= D, test( C, D ) ) ;;
(occ rule) [+test( A, C )#1][+test( A, B )<idx#0>] --> ( D %= A, E %= D, test( C, D ) ) ;;
----------
----------
