----------
From line 1 to 3
----------
test<test( A, B )> { test<test( A, C )> } --> built-in<( my_func( B ) + 4 )>
----------
(constraint) test( ( ?int ), ( ?int ) ), indexes: { <0> }, persistent
(rule) test( A, B ), test( A, C ) <=> ( A == B ) | A %= ( my_func( B ) + 4 ) ;;
(occ rule) [-test( A, B )#0][( A == B ), -test( A, C )<idx#0>] --> A %= ( my_func( B ) + 4 ) ;;
(occ rule) [-test( A, C )#1][-test( A, B )<idx#0>, ( A == B )] --> A %= ( my_func( B ) + 4 ) ;;
----------
----------
