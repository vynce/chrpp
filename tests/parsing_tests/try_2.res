----------
From line 1 to 4
----------
test<test( A, B )> { test<test( C, B )> } --> built-in<2>
----------
(constraint) test( ( ?int ), ( ?( std :: vector )< int > ) ), indexes: { <1> }
(rule) test( A, B ), test( C, B ) <=> ( A == C ) | try_bt( n, A %= 2) ;;
(occ rule) [-test( A, B )#0][-test( C, B )<idx#0>, ( A == C )] --> try_bt( n, A %= 2) ;;
(occ rule) [-test( C, B )#1][-test( A, B )<idx#0>, ( A == C )] --> try_bt( n, A %= 2) ;;
----------
----------
