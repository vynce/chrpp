----------
From line 1 to 4
----------
test<test( A, B )> { test<test( C, B )> } --> built-in<begin(  )>
----------
(constraint) test( ( ?int ), ( ?( std :: vector )< int > ) ), indexes: { <1> }
(rule) test( A, B ), test( C, B ) <=> ( A == C ) | ( __it_n = ( B . begin(  ) ), __it_end_n = ( B . end(  ) ), __local_success__3 = false, behavior( ( __local_success__3 || ( __it_n == __it_end_n ) ), __local_success__3 = true, ( ++__it_n ), __local_success__3, , , ( n = ( *__it_n ), A %= 2 )) ) ;;
(occ rule) [-test( A, B )#0][-test( C, B )<idx#0>, ( A == C )] --> ( __it_n = ( B . begin(  ) ), __it_end_n = ( B . end(  ) ), __local_success__3 = false, behavior( ( __local_success__3 || ( __it_n == __it_end_n ) ), __local_success__3 = true, ( ++__it_n ), __local_success__3, , , ( n = ( *__it_n ), A %= 2 )) ) ;;
(occ rule) [-test( C, B )#1][-test( A, B )<idx#0>, ( A == C )] --> ( __it_n = ( B . begin(  ) ), __it_end_n = ( B . end(  ) ), __local_success__3 = false, behavior( ( __local_success__3 || ( __it_n == __it_end_n ) ), __local_success__3 = true, ( ++__it_n ), __local_success__3, , , ( n = ( *__it_n ), A %= 2 )) ) ;;
----------
----------
