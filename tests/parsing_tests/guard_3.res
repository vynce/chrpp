----------
From line 1 to 3
----------
test<test( A, B )> { test<test( A, C )> } -->
----------
(constraint) test( ( ?int ), ( ?int ) ), indexes: { <0> }, persistent
(rule) test( A, B ), test( A, C ) <=> my_func( A, C ) | success ;;
(occ rule) [-test( A, B )#0][-test( A, C )<idx#0>, my_func( A, C )] --> success ;;
(occ rule) [-test( A, C )#1][my_func( A, C ), -test( A, B )<idx#0>] --> success ;;
----------
----------
