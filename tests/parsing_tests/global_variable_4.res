----------
From line 1 to 4
----------
test<test( A, B )> { test<test( A, C )> } --> built-in<( D :: my_function )( A )>
----------
(constraint) test( ( ?int ), ( ?int ) ), indexes: { <0> }, persistent
(rule) test( A, B ), test( A, C ) ==> ( D :: my_function )( A ) ;;
(occ rule) [+test( A, B )#0][+test( A, C )<idx#0>] --> ( D :: my_function )( A ) ;;
(occ rule) [+test( A, C )#1][+test( A, B )<idx#0>] --> ( D :: my_function )( A ) ;;
----------
----------
