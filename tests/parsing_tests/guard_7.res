----------
From line 1 to 4
----------
test<test( A, B )> { test<test( A, C )> } -->
----------
(constraint) test( ( ?int ), ( ?int ) ), indexes: { <0> }, persistent
(rule) test( A, B ), test( A, C ) ==> ( c = ( A + 5 ) ), ( D == 2 ) | success ;;
(occ rule) [+test( A, B )#0][( c = ( A + 5 ) ), ( D == 2 ), +test( A, C )<idx#0>] --> success ;;
(occ rule) [+test( A, C )#1][( c = ( A + 5 ) ), ( D == 2 ), +test( A, B )<idx#0>] --> success ;;
----------
----------
