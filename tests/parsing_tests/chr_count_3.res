----------
From line 1 to 4
----------
test<test( A, B )> { test<test( C, B )> } --> test<test( _, _ )>
----------
(constraint) test( ( ?int ), ( ?( std :: vector )< int > ) ), indexes: { <1> }, persistent
(rule) test( A, B ), test( C, B ) <=> chr_count( test( _, _ ) ) ;;
(occ rule) [-test( A, B )#0][-test( C, B )<idx#0>] --> chr_count( test( _, _ ) ) ;;
(occ rule) [-test( C, B )#1][-test( A, B )<idx#0>] --> chr_count( test( _, _ ) ) ;;
----------
----------
