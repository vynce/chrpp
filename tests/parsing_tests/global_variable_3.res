----------
From line 1 to 5
----------
test<test( A, B )> { test<test( A, C )> } --> test<test( D, ( bh . Toto ) )>
----------
(constraint) test( ( ?int ), ( ?int ) ), indexes: { <0> }, persistent
(rule) test( A, B ), test( A, C ) ==> ( A == D ) | test( D, ( bh . Toto ) ) ;;
(occ rule) [+test( A, B )#0][( A == D ), +test( A, C )<idx#0>] --> test( D, ( bh . Toto ) ) ;;
(occ rule) [+test( A, C )#1][( A == D ), +test( A, B )<idx#0>] --> test( D, ( bh . Toto ) ) ;;
----------
----------
