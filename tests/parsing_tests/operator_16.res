----------
From line 1 to 3
----------
test<test( A, B )> { test<test( A, C )> } --> built-in<( A != B )>
----------
(constraint) test( ( ?int ), ( ?int ) ), indexes: { <0> }, persistent
(rule) test( A, B ), test( A, C ) <=> ( A != B ) | C %= ( A != B ) ;;
(occ rule) [-test( A, B )#0][( A != B ), -test( A, C )<idx#0>] --> C %= ( A != B ) ;;
(occ rule) [-test( A, C )#1][-test( A, B )<idx#0>, ( A != B )] --> C %= ( A != B ) ;;
----------
----------
