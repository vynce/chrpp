----------
From line 1 to 4
----------
test<test( A, B )> { test<test( A, C )> } -->
----------
(constraint) test( ( ?int ), ( ?int ) ), indexes: { <0> }, persistent
(rule) test( A, B ), test( A, C ) <=> ( B == A ) | success ;;
(occ rule) [-test( A, C )#0][-test( A, B )<idx#0>#{passive,bang}, ( B == A )] --> success ;;
----------
----------
