----------
From line 1 to 3
----------
test<test( A, C, a )> { test<test( 1, B, 'x' )> } -->
----------
(constraint) test( ( ?int ), ( -int ), ( +unsigned char ) ), indexes: { <0,2>, <2> }, persistent
(rule) test( A, C, a ), test( 1, B, 'x' ) <=> success ;;
(occ rule) [-test( A, C, a )#0][-test( 1, B, 'x' )<idx#0>] --> success ;;
(occ rule) [-test( 1, B, 'x' )#1][-test( A, C, a )<idx#1>] --> success ;;
----------
----------
