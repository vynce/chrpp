----------
From line 1 to 4
----------
test<test( A, B )> { test<test( C, B )> } --> test<test( A, _ )>
----------
(constraint) test( ( ?int ), ( ?( std :: vector )< int > ) ), indexes: { <0>, <1> }, persistent
(rule) test( A, B ), test( C, B ) <=> chr_count( test( A, _ ) ) ;;
(occ rule) [-test( A, B )#0][-test( C, B )<idx#1>] --> chr_count( test( A, _ ) ) ;;
(occ rule) [-test( C, B )#1][-test( A, B )<idx#1>] --> chr_count( test( A, _ ) ) ;;
----------
----------
