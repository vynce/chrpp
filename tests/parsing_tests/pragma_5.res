----------
From line 1 to 4
----------
test<test( A, B )> { test<test( A, C )> } -->
----------
(constraint) test( ( ?int ), ( ?int ) ), indexes: { <0> }, persistent
(rule) test( A, B ), test( A, C ) <=> ( B == A ) | ( A + C ) # catch_failure ;;
(occ rule) [-test( A, B )#0][( B == A ), -test( A, C )<idx#0>] --> ( A + C ) # catch_failure ;;
(occ rule) [-test( A, C )#1][-test( A, B )<idx#0>, ( B == A )] --> ( A + C ) # catch_failure ;;
----------
----------
