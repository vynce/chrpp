----------
From line 1 to 4
----------
chrtest<chrtest( A, D )> -->
test<test( A, B )> { test<test( A, C )> } --> chrtest<chrtest( A, D )>
----------
(constraint) test( ( ?int ), ( ?int ) ), indexes: { <0> }, persistent
(constraint) chrtest( ( ?int ), ( ?int ) ), persistent
(rule) test( A, B ), test( A, C ) ==> chrtest( A, D ) ;;
(occ rule) [+test( A, B )#0][+test( A, C )<idx#0>] --> chrtest( A, D ) ;;
(occ rule) [+test( A, C )#1][+test( A, B )<idx#0>] --> chrtest( A, D ) ;;
----------
----------
