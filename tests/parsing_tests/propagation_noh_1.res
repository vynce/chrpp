----------
From line 1 to 4
----------
test<test( A, B )> { test<test( A, C )> } -->
----------
(constraint) test( ( ?int ), ( ?int ) ), indexes: { <0> }, persistent
(rule) test( A, B ), test( A, C ) =>> ( A == B ) | success ;;
(occ rule) [+test( A, B )#0#no_history][( A == B ), +test( A, C )<idx#0>#no_history] --> success ;;
(occ rule) [+test( A, C )#1#no_history][+test( A, B )<idx#0>#no_history, ( A == B )] --> success ;;
----------
----------
