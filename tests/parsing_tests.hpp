/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <array>
#include <fstream>
#include <sstream>
#include <string>
#include <cerrno>

#include <chrppc/chrppc_parse_build.hpp>
#include <trace.hh>

namespace tests {
	namespace compiler {

	std::string get_file_contents(const char *file_name)
	{
	  std::ifstream in(file_name, std::ios::in | std::ios::binary);
	  if (in)
	  {
	    std::ostringstream contents;
	    contents << in.rdbuf();
	    in.close();
	    return(contents.str());
	  }
	  throw(errno);
	}

	template < unsigned int num_test >
	void compiler_parsing_test< num_test >::create()
	{
		std::string test_name = name;
		std::string test_file_name = std::string(TESTS_PATH) + "/parsing_tests/" + test_name + ".chr";
		std::string result_file_name = std::string(TESTS_PATH) + "/parsing_tests/" + test_name + ".res";

		// -------------------------------------------------------------------------------
		// Build CHR test program
		std::string str_content;
		str_content += "<CHR>\n";
		try
		{
			str_content += get_file_contents(test_file_name.c_str());
		}
		catch(...)
		{
			FAILM("Can't read file " + test_file_name);
		}
		str_content += "</CHR>\n";

		TRACE( std::cout << str_content; )

		test_file_name = test_file_name.substr(test_file_name.find_last_of('/')+1);
		TAO_PEGTL_NAMESPACE::string_input in( str_content, test_file_name );
		std::vector< chr::compiler::parser::ast_builder::AstProgramBuilder > chr_prg_results;
		std::ostringstream oss_err;
		auto ret = chr::compiler::parse_chr_input(in, chr_prg_results, test_file_name, oss_err);

		// -------------------------------------------------------------------------------
		// Extract results
		std::string test_result;
		for (auto& prg_res : chr_prg_results)
		{
			chr::compiler::visitor::ProgramPrint vpp;
			test_result += "----------\n";
			test_result += "From line " + std::to_string(prg_res._start_line_number) + " to " + std::to_string(prg_res._end_line_number) + "\n";
			test_result += "----------\n";
			if (ret != 0)
			{
				test_result += oss_err.str();
				test_result += "----------\n";
			} else {
				test_result += prg_res._str_dependency_graph;
				test_result += "----------\n";
				test_result += std::string(vpp.string_from(prg_res.prg));
			}
			test_result += "----------\n";
			test_result += "----------\n";
		}
		TRACE( std::cout << test_result; )

		// -------------------------------------------------------------------------------
		// Compare with recorded result
		std::string recorded_result;
		try
		{
			recorded_result += get_file_contents(result_file_name.c_str());
		}
		catch(...)
		{
			FAILM("Can't read file " + result_file_name);
		}
		TRACE( std::cout << "Length test result: " << test_result.size() << std::endl; )
		TRACE( std::cout << "Length recorded result: " << recorded_result.size() << std::endl; )
		ASSERT(recorded_result == test_result);
	}

	}
}
