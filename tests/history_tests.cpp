/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <array>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_set>
#include <cerrno>

#include <runtime_tests.hh>
#include <chrpp.hh>

namespace tests {
	namespace history {

		/**
		 * Check just some simple add to history
		 */
		void check1()
		{
			chr::History< 4 > his;

			ASSERT( his.check( {{ 3, 1, 5, 8 }} ) );
			ASSERT( !his.check( {{ 3, 1, 5, 8}} ) );
			ASSERT( !his.check( {{ 3, 8, 5, 1}} ) );
		}

		/**
		 * Check that history remembers many checks
		 */
		void checks()
		{
			chr::History< 3 > his;

			for (int i = 0; i < 10; ++i)
				for (int j = i+1; j < 10; ++j)
					for (int k = j+1; k < 10; ++k)
					{
						chr::History< 3 >::Data_type e;
						e[0] = i; e[1] = j; e[2] = k;
						ASSERT( his.check( e ) );
						ASSERT( !his.check( e ) );
					}
			for (int i = 0; i < 10; ++i)
				for (int j = 0; j < 10; ++j)
					for (int k = 0; k < 10; ++k)
						if ((i != j) && (j != k) && (i != k))
						{
							chr::History< 3 >::Data_type e;
							e[0] = i; e[1] = j; e[2] = k;
							ASSERTM("("+std::to_string(i)+","+std::to_string(j)+","+std::to_string(k)+")",!his.check( e ) );
						}
		}

		/**
		 * *************************************************************
		 * Data structures needed to make some tests on Constraint_store
		 * *************************************************************
		 */
		using DummyConstraint = std::tuple<unsigned long int>;

		/**
		 * Check that History rewind when backtrack.
		 */
		void backtrack_tests()
		{
			using namespace chr;
			auto cs = chr::make_shared< chr::Constraint_store_simple<DummyConstraint> >();
			auto his = chr::make_shared< chr::History_dyn< 2 > >();
			std::set< unsigned int > ids;

			for (unsigned int i=1; i < 11; ++i)
			{
				auto ci = DummyConstraint(i);
				ASSERT(std::get<0>(*cs->add( ci )) == i);
				ids.insert(i);
			}
			TRACE( std::cout << "History size: " << his->size() << std::endl; )
			ASSERT(his->size() == 0);
			ASSERT(Backtrack::callback_count() == 0);
			ASSERT(Backtrack::depth() == 0);
			Depth_t d = Backtrack::depth();
			assert(d == 0);

			unsigned int level = 0;
			auto it1 = cs->begin();
			for ( ; !it1.at_end(); ++it1)
			{
				auto it2 = it1;
				++it2;
				for ( ; !it2.at_end(); ++it2)
				{
					if ((level % 10) == 0)
					{
						Backtrack::inc_backtrack_depth();
						++d;
						TRACE( std::cout << "New choice, depth = " << d << std::endl; )
					}
					chr::History_dyn< 2 >::Data_type e;
					e[0] = std::get<0>(*it1);
					e[1] = std::get<0>(*it2);
					TRACE( std::cout << "--> " << std::get<0>(*it1) << " " << std::get<0>(*it2) << std::endl; )
					ASSERT( his->check( e ) );
					TRACE( std::cout << "History: " << his->to_string() << std::endl; )
					++level;
				}
			}
			TRACE( std::cout << "Add elements, History size: " << his->size() << std::endl; )
			TRACE( std::cout << "History: " << his->to_string() << std::endl; )
			ASSERT(his->size() == 45);
			d -= 2;
			Backtrack::back_to(d);
			TRACE( std::cout << "Back to depth " << d << std::endl; )
			TRACE( std::cout << "History: " << his->to_string() << std::endl; )
			ASSERT(his->size() == (d * 10));
			d -= 1;
			Backtrack::back_to(d);
			TRACE( std::cout << "Back to depth " << d << std::endl; )
			TRACE( std::cout << "History: " << his->to_string() << std::endl; )
			ASSERT(his->size() == (d * 10));
			d = 0;
			Backtrack::back_to(d);
			TRACE( std::cout << "Back to depth " << d << std::endl; )
			TRACE( std::cout << "History: " << his->to_string() << std::endl; )
			ASSERT(his->size() == (d * 10));
		}
	}

	void run_history_tests(cute::suite& s)
	{
		s.push_back( { "History, first check test", history::check1 } );
		s.push_back( { "History, check tests", history::checks } );
		s.push_back( { "History dynamic, backtrack tests", history::backtrack_tests } );
	}
}
