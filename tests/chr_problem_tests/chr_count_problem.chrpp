/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <array>
#include <fstream>
#include <sstream>
#include <string>
#include <cerrno>

#include <chr_test_problems.hh>
#include <chrpp.hh>

<CHR name="ChrCount">
	<chr_constraint> a(+int, ?int), count1(?std::size_t), count2(?std::size_t), count3(?std::size_t,?int), count4(?std::size_t,?int), count5(?std::size_t)
	count1(R) ==> R %= chr_count(a(3,_));;
	count2(R) ==> R %= chr_count(a(2,_));;
	count3(R,X) ==> R %= chr_count(a(_,X));;
	count4(R,X) ==> R %= chr_count(a(3,X));;
	count5(R) ==> R %= chr_count(a(_,_));;
</CHR>

namespace tests {
	/**
	 * Run chr_count test
	 */
	void chr_count()
	{
		chr::reset();
		auto space = ChrCount::create();
		chr::Logical_var< int > X, Y;
		chr::Logical_var< std::size_t > R1, R2, R3, R4, R5;
		space->a(3,X);
		space->a(3,Y);
		space->a(2,X);
		space->count1(R1);
		ASSERT(R1.ground() && (*R1 == 2));
		space->count2(R2);
		ASSERT(R2.ground() && (*R2 == 1));
		space->count3(R3,X);
		ASSERT(R3.ground() && (*R3 == 2));
		space->count4(R4,X);
		ASSERT(R4.ground() && (*R4 == 1));
		space->count5(R5);
		ASSERT(R5.ground() && (*R5 == 3));
	}

	void run_chr_count_problems(cute::suite& s)
	{
		s.push_back( { "chr_count test", chr_count } );
	}
}
