/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the Apache License, Version 2.0.
 *
 *  Copyright:
 *     2016, Vincent Barichard <Vincent.Barichard@univ-angers.fr>
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 *
 */

#include <array>
#include <list>
#include <vector>
#include <utility>
#include <fstream>
#include <sstream>
#include <string>
#include <cerrno>

#include <chr_test_problems.hh>
#include <chrpp.hh>

struct ContainerPairHelper
{
    bool check(chr::Logical_pair< chr::Logical_var< int >, chr::Logical_var< int > > p) const
    {
        return p.get().first.ground();
    }

    bool ok()
    {
        ++_count;
        return true;
    }

    unsigned int _count = 0;
};

<CHR name="ContainerPair" parameters="ContainerPairHelper& ch">
    <chr_constraint> my_constraint(-chr::Logical_pair<[ chr::Logical_var<[ int ]>, chr::Logical_var<[ int ]> ]>)
    my_constraint(X) <=> ch.check(*X) | ch.ok();;
</CHR>

namespace tests {
	/**
	 * Tests about container pair
	 */
	void container_pair_tests()
	{
		chr::reset();
		ContainerPairHelper ph;
		auto space = ContainerPair::create(ph);
	    chr::Logical_var< int > X, Y;
	    chr::Logical_pair< chr::Logical_var< int >, chr::Logical_var< int > > p(X,Y);
	    space->my_constraint(p);
	    ASSERT(ph._count == 0);
	    X %= 4;
	    ASSERT(X.ground());
	    ASSERT(*X == 4);
	    ASSERT(ph._count == 1);
	}
}

struct ContainerVectorHelper
{
    bool check(chr::Logical_vector< chr::Logical_var< int > > v) const
	{
		std::vector< chr::Logical_var< int > >& vi = v;
		return vi.front().ground();
	}

	bool ok()
	{
		++_count;
		return true;
	}

	unsigned int _count = 0;
};

<CHR name="ContainerVector" parameters="ContainerVectorHelper& ch">
    <chr_constraint> my_constraint(-chr::Logical_vector<[ chr::Logical_var<[ int ]> ]>)
	my_constraint(X) <=> ch.check(*X) | ch.ok();;
</CHR>

namespace tests {
	/**
	 * Tests about container vector
	 */
	void container_vector_tests()
	{
		chr::reset();
		ContainerVectorHelper ch;
		auto space = ContainerVector::create(ch);
		chr::Logical_var< int > X, Y, Z;
        chr::Logical_vector< chr::Logical_var< int > > v({ X, Y, Z});
		space->my_constraint(v);
		ASSERT(ch._count == 0);
		X %= 4;
		ASSERT(X.ground());
		ASSERT(*X == 4);
		ASSERT(ch._count == 1);
	}
}

struct ContainerListHelper
{
    bool check(chr::Logical_list< chr::Logical_var< int > > v) const
	{
		std::list< chr::Logical_var< int > >& vi = v;
		return vi.front().ground();
	}

	bool ok()
	{
		++_count;
		return true;
	}

	unsigned int _count = 0;
};

<CHR name="ContainerList" parameters="ContainerListHelper& ch">
    <chr_constraint> my_constraint(-chr::Logical_list<[ chr::Logical_var<[ int ]> ]>)
	my_constraint(X) <=> ch.check(*X) | ch.ok();;
</CHR>

namespace tests {
	/**
	 * Tests about container vector
	 */
	void container_list_tests()
	{
		chr::reset();
		ContainerListHelper ch;
		auto space = ContainerList::create(ch);
		chr::Logical_var< int > X, Y, Z;
		chr::Logical_list< chr::Logical_var< int > > v({ X, Y, Z});
		space->my_constraint(v);
		ASSERT(ch._count == 0);
		X %= 4;
		ASSERT(X.ground());
		ASSERT(*X == 4);
		ASSERT(ch._count == 1);
	}

    void run_logical_structure_problems(cute::suite& s)
	{
        s.push_back( { "Logical_pair", container_pair_tests } );
        s.push_back( { "Logical_vector", container_vector_tests } );
        s.push_back( { "Logical_list", container_list_tests } );
    }
}
